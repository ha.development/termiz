﻿using System.ComponentModel;
using TermizApp.Controls.Customs;
using TermizApp.iOS.Renders;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;


[assembly: ExportRenderer(typeof(BorderlessEntryCustom), typeof(BorderlessEntryCustomiOS))]
namespace TermizApp.iOS.Renders
{
    public class BorderlessEntryCustomiOS : EntryRenderer
    {
        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            Control.Layer.BorderWidth = 0;
            Control.BorderStyle = UITextBorderStyle.None;
        }
    }
}