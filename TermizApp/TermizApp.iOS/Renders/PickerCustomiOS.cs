﻿using System;
using System.Drawing;
using TermizApp.iOS.Renders;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(Picker), typeof(PickerCustomiOS))]
namespace TermizApp.iOS.Renders
{
    public class PickerCustomiOS : PickerRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                Control.Layer.CornerRadius = 3;
                Control.Layer.BorderColor = UIColor.FromRGB(89, 122, 176).CGColor;
                Control.TextColor = UIColor.FromRGB(89, 122, 176);
                Control.TintColor = UIColor.FromRGB(89, 122, 176);
                Control.AttributedPlaceholder = new Foundation.NSAttributedString(this.Control.AttributedPlaceholder.Value, foregroundColor: UIColor.FromRGB(89, 122, 176));
                var font = UIFont.FromName("Ubuntu", 9f);
                Control.Font = font;
                Control.Layer.BorderWidth = (nfloat).7;
                Control.ClipsToBounds = true;
                Control.RightViewMode = UITextFieldViewMode.Always;

                var imageView = new UIImageView(UIImage.FromBundle("drop"))
                {
                    // Indent it 10 pixels from the left.
                    Frame = new RectangleF(-5, 0, 10, 10)
                };

                UIView objLeftView = new UIView(new System.Drawing.Rectangle(0, 0, 10, 10));
                objLeftView.AddSubview(imageView);
                Control.RightView = objLeftView;
            }
        }

        
    }
}