﻿using CoreAnimation;
using CoreGraphics;
using Foundation;
using TermizApp.Controls;
using TermizApp.iOS.Renders;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(ProgressBar), typeof(ProgressBarCustomiOS))]
namespace TermizApp.iOS.Renders
{
    public class ProgressBarCustomiOS : ProgressBarRenderer
    {

        protected override void OnElementChanged( ElementChangedEventArgs<Xamarin.Forms.ProgressBar> e)
        {
            base.OnElementChanged(e);
            var gradientLayer = new CAGradientLayer();
            gradientLayer.Colors = new[] { UIColor.Red.CGColor, UIColor.Blue.CGColor };
            gradientLayer.Locations = new NSNumber[] { 0, 1 };
            gradientLayer.Frame = this.Frame;

            this.BackgroundColor = UIColor.Clear;
            this.Layer.AddSublayer(gradientLayer);
            Control.ProgressTintColor = Color.FromRgb(182, 231, 233).ToUIColor();
            Control.TrackTintColor = Color.FromRgb(188, 203, 219).ToUIColor();
        }


        public override void LayoutSubviews()
        {
            base.LayoutSubviews();
            var X = 1.0f;
            var Y = 7.0f;
            CGAffineTransform transform = CGAffineTransform.MakeScale(X, Y);
            this.Transform = transform;
            this.ClipsToBounds = true;
            this.Layer.MasksToBounds = true;
            this.Layer.CornerRadius = 20;
        }
    }
}