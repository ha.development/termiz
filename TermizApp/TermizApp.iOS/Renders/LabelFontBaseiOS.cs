﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using TermizApp.Controls.Bases;
using TermizApp.iOS.Renders;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(LabelFontBase), typeof(LabelFontBaseiOS))]
namespace TermizApp.iOS.Renders
{
    public class LabelFontBaseiOS : LabelRenderer
    {

        private readonly static string TAG = nameof(LabelFontBaseiOS);

        protected override void Dispose(bool disposing)
        {
            try
            {
                base.Dispose(disposing);
            }
            catch (NullReferenceException ex)
            {
                Debug.WriteLine(ex.Message, TAG);
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if (Control == null || Element == null) return;
            
            var font = Abstractions.FontExtensions.FindNameForFont(((LabelFontBase)Element).FontLabel);
            if(!string.IsNullOrEmpty(font))
            {
                Control.Font = UIFont.FromName(font, (nfloat)Element.FontSize);
            }
        }
    }
}