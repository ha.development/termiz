﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using TermizApp.Controls.Bases;
using TermizApp.iOS.Renders;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(ButtonFontBase), typeof(ButtonFontBaseiOS))]
namespace TermizApp.iOS.Renders
{
    public class ButtonFontBaseiOS : ButtonRenderer
    {
        private readonly static string TAG = nameof(ButtonFontBaseiOS);

        protected override void Dispose(bool disposing)
        {
            try
            {
                base.Dispose(disposing);
            }
            catch (NullReferenceException ex)
            {
                Debug.WriteLine(ex.Message, TAG);
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if (Control == null || Element == null) return;

            var font = Abstractions.FontExtensions.FindNameForFont(((ButtonFontBase)Element).FontLabel);
            if (!string.IsNullOrEmpty(font))
            {
                Control.Font = UIFont.FromName(font, (nfloat)Element.FontSize);
            }
        }
    }
}