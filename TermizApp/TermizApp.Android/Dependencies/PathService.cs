﻿using System;
using System.IO;
using TermizApp.Droid.Dependencies;
using TermizApp.Helpers;
using TermizApp.Settings;
using Xamarin.Forms;

[assembly: Dependency(typeof(PathService))]
namespace TermizApp.Droid.Dependencies
{
    public class PathService : IPathService
    {
        public string GetDatabasePath()
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            return Path.Combine(path, AppConfiguration.Values.NameDB);
        }
    }
}
