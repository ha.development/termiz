﻿using Android.Content;
using TermizApp.Controls.Customs;
using TermizApp.Droid.Renders;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(BorderlessEntryCustom), typeof(BorderlessEntryCustomAndroid))]
namespace TermizApp.Droid.Renders
{
    public class BorderlessEntryCustomAndroid : EntryRenderer
    {
        public BorderlessEntryCustomAndroid(Context context) : base(context)
        {
        }
        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Entry> e)
        {
            base.OnElementChanged(e);
            if (e.OldElement == null)
            {
                Control.Background = null;
            }
        }
    }
}