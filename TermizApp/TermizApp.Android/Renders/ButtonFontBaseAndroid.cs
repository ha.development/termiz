﻿using Android.Content;
using Android.Graphics;
using Android.Views;
using TermizApp.Controls.Bases;
using TermizApp.Droid.Renders;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(ButtonFontBase), typeof(ButtonFontBaseAndroid))]
namespace TermizApp.Droid.Renders
{
    public class ButtonFontBaseAndroid : ButtonRenderer
    {

        private readonly Context context;

        public ButtonFontBaseAndroid(Context context) : base(context)
        {
            this.context = context;
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Button> e)
        {
            base.OnElementChanged(e);
            if (e.OldElement == null)
            {
                //Only enable hardware accelleration on lollipop
                if ((int)Android.OS.Build.VERSION.SdkInt < 21)
                {
                    SetLayerType(LayerType.Software, null);
                }
                var font = TermizApp.Abstractions.FontExtensions.FindNameFileForFont(((ButtonFontBase)Element).FontLabel);
                if (!string.IsNullOrEmpty(font))
                {
                    Control.Typeface = Typeface.CreateFromAsset(context.Assets, font);
                }

            }
        }
    }
}