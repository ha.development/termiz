﻿using Android.Content;
using TermizApp.Controls;
using TermizApp.Droid.Renders;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(ProgressBar), typeof(ProgressBarCustomAndroid))]
namespace TermizApp.Droid.Renders
{
    public class ProgressBarCustomAndroid : ProgressBarRenderer
    {

        public ProgressBarCustomAndroid(Context context) : base(context)
        {
        }
        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.ProgressBar> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                var progressBar = Control as Android.Widget.ProgressBar;
                var draw = Resources.GetDrawable(Resource.Drawable.bar_color);
                progressBar.ProgressDrawable = draw;
            }
        }

    }
}