﻿using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Support.V4.Content;
using Android.Util;
using Android.Views;
using Android.Views.InputMethods;
using System;
using System.ComponentModel;
using TermizApp.Droid.Renders;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(Picker), typeof(PickerCustomAndroid))]
namespace TermizApp.Droid.Renders
{
    public class PickerCustomAndroid : PickerRenderer
    {
      
        public PickerCustomAndroid(Context context) : base(context)
        {
            
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null || e.NewElement == null || Control == null)
                return;

            if (Control != null)
            {
                var view = (Picker)Element;
                Control.ImeOptions = (ImeAction)ImeFlags.NoExtractUi;
                Control.SetCompoundDrawablesWithIntrinsicBounds(null, null, GetDrawable("selectorW", 25, 25), null);
                Control.SetPadding(15, 5, 15, 5);
                Control.SetTextColor(Android.Graphics.Color.ParseColor("#597ab0"));
                Control.SetTextSize(Android.Util.ComplexUnitType.Sp, 9);
                Control.SetHintTextColor(Android.Graphics.Color.ParseColor("#597ab0"));
                var _gradientBackground = new GradientDrawable();
                _gradientBackground.SetShape(ShapeType.Rectangle);
                _gradientBackground.SetColor(view.BackgroundColor.ToAndroid());
                // Thickness of the stroke line  
                _gradientBackground.SetStroke(2, Android.Graphics.Color.ParseColor("#597ab0"));
                // Radius for the curves 
                _gradientBackground.SetCornerRadius(DpToPixels(this.Context, Convert.ToSingle(3)));

                Control.SetMaxLines(1);
                Control.SetBackground(_gradientBackground);
                Control.SetTextColor(Element.TextColor.ToAndroid());
                Control.RootView.SetBackgroundColor(Android.Graphics.Color.Transparent);
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName.Equals(nameof(Element.IsEnabled)) && Control != null)
                Control.SetTextColor(Element.TextColor.ToAndroid());
        }

        BitmapDrawable GetDrawable(string name, int width, int height)
        {
            int resID = Resource.Drawable.drop;
            var drawable = ContextCompat.GetDrawable(MainActivity.Current, resID);
            var bitmap = ((BitmapDrawable)drawable).Bitmap;

            return new BitmapDrawable(MainActivity.Current.Resources, Bitmap.CreateScaledBitmap(bitmap, width, height, false));
        }

        static float DpToPixels(Context context, float valueInDp)
        {
            DisplayMetrics metrics = context.Resources.DisplayMetrics;
            return TypedValue.ApplyDimension(ComplexUnitType.Dip, valueInDp, metrics);
        }

    }
}