﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using TermizApp.Controls.Bases;
using TermizApp.Droid.Renders;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(LabelFontBase), typeof(LabelFontBaseAndroid))]
namespace TermizApp.Droid.Renders
{
    public class LabelFontBaseAndroid : LabelRenderer
    {

        private readonly Context context;

        public LabelFontBaseAndroid(Context context) : base(context)
        {
            this.context = context;
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Label> e)
        {
            base.OnElementChanged(e);
            if (e.OldElement == null)
            {
                //Only enable hardware accelleration on lollipop
                if ((int)Android.OS.Build.VERSION.SdkInt < 21)
                {
                    SetLayerType(LayerType.Software, null);
                }
                var font = TermizApp.Abstractions.FontExtensions.FindNameFileForFont(((LabelFontBase)Element).FontLabel);
                if(!string.IsNullOrEmpty(font))
                {
                    Control.Typeface = Typeface.CreateFromAsset(context.Assets, font);
                }
                
            }
        }
    }
}