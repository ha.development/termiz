﻿using TermizApp.Models.Bases;

namespace TermizApp.Abstractions
{
    public static class FontExtensions
    {

        public static string FindNameFileForFont(Fonts font)
        {
            switch (font)
            {
                case Fonts.Ubuntu:
                    return "Ubuntu-R.ttf";

                case Fonts.QuicksandLight:
                    return "Quicksand-Light.ttf";

                default:
                    return string.Empty;
            }
        }

        public static string FindNameForFont(Fonts font)
        {
            switch (font)
            {
                case Fonts.Ubuntu:
                    return "Ubuntu";

                case Fonts.QuicksandLight:
                    return "Quicksand-Light";

                default:
                    return string.Empty;
            }
        }

    }
}
