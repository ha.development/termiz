﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Plugin.Connectivity.Abstractions;
using Prism.AppModel;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Navigation;
using TermizApp.LocalData;

namespace TermizApp.Abstractions
{
    public class ViewModelBase : BindableBase, INavigationAware, IDestructible, IPageLifecycleAware
    {

        #region Vars
        private readonly static string TAG = nameof(ViewModelBase);
        protected INavigationService NavigationService { get; private set; }
        protected IUserDialogs UserDialogsService { get; private set; }
        protected IConnectivity Connectivity { get; set; }
        protected IEventAggregator Events { get; set; }
        #endregion

        #region Vars Commands
        public DelegateCommand ReturnToPreviousPageCommand { get; private set; }
        public DelegateCommand ShowMenuCommand { get; set; }
        public DelegateCommand CloseSessionCommand { get; set; }
        #endregion

        #region Properties
        private string titleToolbar;
        public string TitleToolbar
        {
            get => titleToolbar; 
            set => SetProperty(ref titleToolbar, value); 
        }
        private bool isBusy;
        public bool IsBusy
        {
            get => isBusy;
            set => SetProperty(ref isBusy, value);
        }
        #endregion

        #region Constructor
        public ViewModelBase(INavigationService navigationService, IUserDialogs userDialogsService, IConnectivity connectivity, IEventAggregator eventAggregator)
        {
            Events = eventAggregator;
            NavigationService = navigationService;
            UserDialogsService = userDialogsService;
            Connectivity = connectivity;
            ReturnToPreviousPageCommand = new DelegateCommand(OnReturnToPreviousPageCommandExecuted);
            ShowMenuCommand = new DelegateCommand(() => App.Master.IsPresented = true);
            CloseSessionCommand = new DelegateCommand(CloseSessionCommandExecute);
        }
        #endregion

        #region Navigation
        public virtual void OnNavigatedFrom(INavigationParameters parameters) { }

        public virtual void OnNavigatedTo(INavigationParameters parameters) { }

        public virtual void OnNavigatingTo(INavigationParameters parameters) { }
        #endregion

        #region Methods Commands
        public virtual async void OnReturnToPreviousPageCommandExecuted()
        {
            await NavigationService.GoBackAsync();
        }
        public virtual async void CloseSessionCommandExecute()
        {
            try
            {
                Profile.Instance.ClearValues();
                AppSettings.Instance.ClearValues();
                await NavigationService.NavigateAsync(new Uri("http://termiz.com/Navigation/LogIn", UriKind.Absolute));
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message, TAG);
            }
        }
        #endregion

        #region Methods
        protected async Task<ResponseBase<T>> RunSafeApi<T>(Task<T> runMethod)
        {
            var result = new ResponseBase<T>
            {
                Status = TypeReponse.Error,
                Response = default(T)
            };
            try
            {
                if (Connectivity.IsConnected)
                {
                    try
                    {
                        result.Response = await runMethod;
                        if (!result.Response.Equals(null))
                        {
                            result.Status = TypeReponse.Ok;
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.Message, TAG);
                    }
                }
                else
                {
                    result.Status = TypeReponse.ErroConnectivity;
                    Debug.WriteLine("Error Connectivity", TAG);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message, TAG);
            }
            return result;
        }
        #endregion

        #region Methods Cycle Life Page
        public virtual void OnAppearing() { }

        public virtual void OnDisappearing() { }

        public virtual void Destroy() { }
        #endregion

    }
}
