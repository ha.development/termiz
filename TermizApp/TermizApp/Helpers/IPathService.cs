﻿namespace TermizApp.Helpers
{
    public interface IPathService
    {
        string GetDatabasePath();
    }
}
