﻿using Prism;
using Prism.Ioc;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Prism.Unity;
using Acr.UserDialogs;
using Plugin.Connectivity.Abstractions;
using Plugin.Settings.Abstractions;
using Plugin.Connectivity;
using Plugin.Settings;
using TermizApp.LocalData;
using SQLite;
using System;
using System.Diagnostics;
using TermizApp.Helpers;
using TermizApp.Views.Home;
using TermizApp.ViewModels.Home;
using TermizApp.Views.Session;
using TermizApp.ViewModels.Session;
using TermizApp.Services.Session;
using TermizApp.Services.Catalogs;
using Prism.Plugin.Popups;
using TermizApp.Views.Popups;
using TermizApp.ViewModels.Popups;
using TermizApp.Views.EfficiencyBoards;
using TermizApp.ViewModels.EfficiencyBoards;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using TermizApp.Views.StartCenter;
using TermizApp.ViewModels.StartCenter;
using TermizApp.Services.Catalogs.GlobalServices;
using System.Threading;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace TermizApp
{
    public partial class App : PrismApplication
    {
        /* 
         * The Xamarin Forms XAML Previewer in Visual Studio uses System.Activator.CreateInstance.
         * This imposes a limitation in which the App class must have a default constructor. 
         * App(IPlatformInitializer initializer = null) cannot be handled by the Activator.
         */
        public App() : this(null) { }

        public App(IPlatformInitializer initializer) : base(initializer) { }

        #region Vars
        public static string TAG = nameof(App);
        public static MasterDetailPage Master { get; set; }
        public static App CurrentInstance { get; private set; }
        public static CancellationTokenSource CancellationToken { get; internal set; }

        public static SQLiteConnection SQLiteConnect;
        #endregion

        protected override async void OnInitialized()
        {
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("MTA1NDE3QDMxMzcyZTMxMmUzMGxlMk5qTVNnaU1neWozcU5jOE43eitHZXVYVzMzNUE1M1hmR1U3VzlRb1U9");
            InitializeComponent();
            CurrentInstance = this;
            AppSettings.Instance.Initialize(Container.Resolve<ISettings>());
            Profile.Instance.Initialize(Container.Resolve<ISettings>());
            InitServiceSQLite();
            if (AppSettings.Instance.Logged)
            {
                await NavigationService.NavigateAsync(new Uri("http://termiz.com/Index/Navigation/StartCenter", UriKind.Absolute));
            }
            else
            {
                await NavigationService.NavigateAsync(new Uri("http://termiz.com/Navigation/LogIn", UriKind.Absolute));
            }
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {

            #region Navigation
            containerRegistry.RegisterForNavigation<MasterPage, MasterPageViewModel>("Index");
            containerRegistry.RegisterForNavigation<NavigationPage>("Navigation");
            #endregion

            #region Pages
            containerRegistry.RegisterForNavigation<MainPage, MainPageViewModel>("Home");
            containerRegistry.RegisterForNavigation<LogInPage, LogInPageViewModel>("LogIn");
            containerRegistry.RegisterForNavigation<TiempoReal, TiempoRealViewModel>("TiempoReal");
            containerRegistry.RegisterForNavigation<Historicos, HistoricosViewModel>("Historicos");
            containerRegistry.RegisterForNavigation<RecoverPasswordPage, RecoverPasswordPageViewModel>("RecoverPassword");
            containerRegistry.RegisterForNavigation<EfficiencyBoardsMenuPage, EfficiencyBoardsMenuPageViewModel>();
            containerRegistry.RegisterForNavigation<GenericBoardPage, GenericBoardPageViewModel>();
            containerRegistry.RegisterForNavigation<GenericEfficiencyPage, GenericEfficiencyPageViewModel>();
            containerRegistry.RegisterForNavigation<StartCenterPage, StartCenterPageViewModel>("StartCenter");
            #endregion

            #region Popups
            containerRegistry.RegisterForNavigation<ProgressReportPopup, ProgressReportPopupViewModel>();
            containerRegistry.RegisterForNavigation<DetailEquimentPopup, DetailEquimentPopupViewModel>("DetailEquiment");
            containerRegistry.RegisterForNavigation<EquipEventsPopup, EquipEventsPopupViewModel>("EquipEvents");            
            #endregion

            #region Instances
            containerRegistry.RegisterInstance(typeof(IUserDialogs), UserDialogs.Instance);
            containerRegistry.RegisterInstance(typeof(IConnectivity), CrossConnectivity.Current);
            containerRegistry.RegisterInstance(typeof(ISettings), CrossSettings.Current);
            containerRegistry.RegisterPopupNavigationService();
            #endregion

            #region Services
            containerRegistry.Register<ISessionService, SessionService>();
            containerRegistry.Register<IProgressReportService, ProgressReportService>();
            containerRegistry.Register<ITiempoRealService, TiempoRealService>();
            containerRegistry.Register<ICatalogosService, CatalogosService>();
            containerRegistry.Register<IHistoricosService, HistoricosService>();
            containerRegistry.Register<IGlobalServices, GlobalServices>();
            #endregion

            #region Repositories
            #endregion
        }

        private void InitServiceSQLite()
        {
            try
            {
                SQLiteConnect = new SQLiteConnection(Container.Resolve<IPathService>().GetDatabasePath());
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message, TAG);
            }
        }

        protected override void OnStart()
        {
            base.OnStart();
            AppCenter.Start("android=a7839b9a-d131-486e-ae90-dbe36fd632cf;" +
                  "ios=26354eb8-3cea-491d-8242-eb95f64fcbbd;",
                  typeof(Analytics), typeof(Crashes));
        }

    }
}
