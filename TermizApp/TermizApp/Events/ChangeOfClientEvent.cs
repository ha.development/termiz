﻿using Prism.Events;

namespace TermizApp.Events
{
    public class ChangeOfClientEvent : PubSubEvent<long>
    { 
    }
}
