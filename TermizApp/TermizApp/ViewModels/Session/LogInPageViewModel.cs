﻿using System;
using System.Diagnostics;
using Acr.UserDialogs;
using Plugin.Connectivity.Abstractions;
using Prism.Commands;
using Prism.Events;
using Prism.Navigation;
using TermizApp.Abstractions;
using TermizApp.Events;
using TermizApp.LocalData;
using TermizApp.Models.Catalogs;
using TermizApp.Services.Session;

namespace TermizApp.ViewModels.Session
{
    public class LogInPageViewModel : ViewModelBase
    {

        #region Vars
        private static string TAG = nameof(LogInPageViewModel);
        private ISessionService services;
        #endregion

        #region Vars Commands
        public DelegateCommand LogInCommand { get; set; }
        public DelegateCommand RecoverPassCommand { get; set; }
        #endregion

        #region Properties
        private string username;
        public string Username
        {
            get { return username; }
            set
            {
                SetProperty(ref username, value);
            }
        }
        private string password;
        public string Password
        {
            get { return password; }
            set
            {
                SetProperty(ref password, value);
            }
        }
        private bool remenber;
        public bool Remenber
        {
            get { return remenber; }
            set
            {
                SetProperty(ref remenber, value);
            }
        }
        private string messageError;
        public string MessageError
        {
            get { return messageError; }
            set
            {
                SetProperty(ref messageError, value);
            }
        }
        private bool showMessageError;
        public bool ShowMessageError
        {
            get { return showMessageError; }
            set
            {
                SetProperty(ref showMessageError, value);
            }
        }
        #endregion

        #region Contructor
        public LogInPageViewModel(ISessionService sessionService, 
                                  INavigationService navigationService, 
                                  IUserDialogs userDialogsService, 
                                  IConnectivity connectivity,
                                  IEventAggregator eventAggregator) : base(navigationService, userDialogsService, connectivity,eventAggregator)
        {
            services = sessionService;
            LogInCommand = new DelegateCommand(LogInCommandExecuted);
            RecoverPassCommand = new DelegateCommand(() => NavigationService.NavigateAsync("RecoverPassword"));
        }
        #endregion

        #region Commands Methods
        private async void LogInCommandExecuted()
        {

#if DEBUG
            //Username = "alexp";
            //Password = "alexp";
#endif

            if (!ValidateInf()) return;

            bool success = false;

            using (UserDialogsService.Loading("Iniciando sesión"))
            {
                var result = await RunSafeApi(services.AuthUserSpartane(Username,Password));
                if(result.Status == TypeReponse.Ok)
                {
                    if(result.Response.SpartanUsers != null && result.Response.RowCount > 0)
                    {
                        var resultUser = await RunSafeApi(services.GetUser(Username));
                        if(resultUser.Status == TypeReponse.Ok && resultUser.Response != null)
                        {
                            ShowMessageError = false;
                            success = true;
                            var user = result.Response.SpartanUsers[0];
                            Profile.Instance.UserName = user.Username;
                            Profile.Instance.Identifier = user.IdUser;
                            Profile.Instance.IsAdmin = !(resultUser.Response.IdCliente > 0);
                            Profile.Instance.IdClient = (long)resultUser.Response.IdCliente;
                            AppSettings.Instance.Logged = true;
                            AppSettings.Instance.RememberUserName = true;
                            await NavigationService.NavigateAsync(new Uri("http://termiz.com/Index/Navigation/StartCenter", UriKind.Absolute));
                        }
                        else
                        {
                            MessageError = "* Credenciales incorrectas.";
                        }
                    }
                    else
                    {
                        MessageError = "* Credenciales incorrectas.";
                    }
                }
                else
                {
                    MessageError = "* Ocurrió un error inténtelo de nuevo.";
                }
            }
            if(!success)
            {
                ShowMessageError = true;
            }
        }
        #endregion

        #region Methods
        private bool ValidateInf()
        {
            if (string.IsNullOrEmpty(Username))
            {
                MessageError = "* Ingresar Usuario";
                ShowMessageError = true;
                return false;
            }
            if (string.IsNullOrEmpty(Password))
            {
                MessageError = "* Ingresar contraseña";
                ShowMessageError = true;
                return false;
            }
            ShowMessageError = false;
            return true;
        }
        #endregion

    }
}
