﻿using Acr.UserDialogs;
using Plugin.Connectivity.Abstractions;
using Prism.Events;
using Prism.Navigation;
using TermizApp.Abstractions;

namespace TermizApp.ViewModels.Session
{
    public class RecoverPasswordPageViewModel : ViewModelBase
    {
        public RecoverPasswordPageViewModel(INavigationService navigationService, IUserDialogs userDialogsService, IConnectivity connectivity, IEventAggregator eventAggregator) : base(navigationService, userDialogsService, connectivity, eventAggregator)
        {
        }
    }
}
