﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using Acr.UserDialogs;
using Microcharts;
using Plugin.Connectivity.Abstractions;
using Prism.Commands;
using Prism.Events;
using Prism.Navigation;
using SkiaSharp;
using Syncfusion.XForms.Buttons;
using TermizApp.Abstractions;
using TermizApp.LocalData;
using TermizApp.Models.Catalogs;
using TermizApp.Services.Catalogs;
using TermizApp.Views.Popups;
using Xamarin.Forms;

namespace TermizApp.ViewModels.Home
{
    public class HistoricosViewModel : ViewModelBase
    {

        #region Vars
        private static string TAG = nameof(MainPageViewModel);
        public ObservableCollectionExt<ProgressReportModel> Items { get; set; }
        public List<Vertica_Region> Regiones = new List<Vertica_Region>();
        public List<Vertica_Historicos> Historicos = new List<Vertica_Historicos>();
        private readonly IProgressReportService _progressReportService;
        public Microcharts.Entry[] listConsumo1 = new Microcharts.Entry[24];
        public Microcharts.Entry[] listConsumo2= new Microcharts.Entry[24];
        public Microcharts.Entry[] listConsumo3 = new Microcharts.Entry[24];
        public Microcharts.Entry[] listConsumo4 = new Microcharts.Entry[24];
        private ITiempoRealService _tiempoRealService;
        private ICatalogosService _catalogosService;
        private IHistoricosService _historicosService;

        public ObservableCollectionExt<Vertica_Region> RegionesList { get; set; }
        public ObservableCollectionExt<Vertica_Sitio> SitiosList { get; set; }
        public ObservableCollectionExt<Vertica_Equipos> EquiposList { get; set; }
        public ObservableCollection<Vertica_Tags> VariablesList { get; set; }

        public ObservableCollectionExt<Options> ConsumosList { get; set; }



        public ObservableCollectionExt<Vertica_ConsumoDemandaHistorico> ListHistoricos { get; set; }

        #endregion

        #region Vars Commands

        public DelegateCommand UpdateConsumosCommand { get; set; }
        public DelegateCommand ShowFiltersCommand { get; set; }
        public DelegateCommand ShowConsumosCommand { get; set; }
        public DelegateCommand ShowOdometrosCommand { get; set; }
        public DelegateCommand ShowTablasCommand { get; set; }
        public DelegateCommand SelectRegionChanged { get; set; }



        public DelegateCommand SelectConsumoCommand { get; set; }
        #endregion

        #region Properties

        private string _ejex;
        public string Ejex
        {
            get { return _ejex; }
            set { SetProperty(ref _ejex, value); }
        }

        private string _periodoActual;
        public string PeriodoActual
        {
            get { return _periodoActual; }
            set { SetProperty(ref _periodoActual, value); }
        }

        private string _periodoAnterior;
        public string PeriodoAnterior
        {
            get { return _periodoAnterior; }
            set { SetProperty(ref _periodoAnterior, value); }
        }

        private string _unidad;
        public string Unidad { get => _unidad; set => SetProperty(ref _unidad, value);}

        private string _variable;
        public string Variable { get => _variable; set => SetProperty(ref _variable, value); }

        private Vertica_Tags _selectedVariable;
        public Vertica_Tags SelectedVariable
        {
            get => _selectedVariable;
            set
            {
                SetProperty(ref _selectedVariable, value);
                if (SelectedVariable != null)
                {
                    UpdateVariables(_selectedVariable);
                    UpdateHistoricos();
                }
            }
        }

        private ObservableCollection<SfSegmentItem> _listaPeriodos = new ObservableCollection<SfSegmentItem>();
        public ObservableCollection<SfSegmentItem> ListaPeriodos
        {
            get { return _listaPeriodos; }
            set { _listaPeriodos = value; }
        }

        private int _selectedPeriodo;
        public int SelectedPeriodo
        {
            get { return _selectedPeriodo; }
            set {
                SetProperty(ref _selectedPeriodo, value);
                UpdateHistoricos();
            }
        }
        private Vertica_Region _selectedRegion;
        public Vertica_Region SelectedRegion
        {
            get { return _selectedRegion; }
            set
            {
                SetProperty(ref _selectedRegion, value);
                GetSitios();
            }
        }

        private Vertica_Sitio _selectedSitio;
        public Vertica_Sitio SelectedSitio
        {
            get { return _selectedSitio; }
            set
            {
                SetProperty(ref _selectedSitio, value);
                GetEquipos();
            }
        }


        private Vertica_Equipos _selectedEquipo;
        public Vertica_Equipos SelectedEquipo
        {
            get { return _selectedEquipo; }
            set
            {
                SetProperty(ref _selectedEquipo, value);
                GetVariables();
            }
        }


        // valores temporales en lo que se invoca del servicio y basado esto se podran colocar de forma fija o dinamiza sgun corresponda
        private static readonly SKColor Color1 = SKColor.Parse("#af7ac5");
        private static readonly SKColor Color2 = SKColor.Parse("#52A1D9");
        private static readonly SKColor Color3 = SKColor.Parse("#e74c3c");
        private static readonly SKColor Color4 = SKColor.Parse("#34495e");
        private static readonly SKColor Color5 = SKColor.Parse("#9c27b0");



        private static readonly SKColor TxtColor = SKColor.Parse("#FFFFFF");



        public Chart BarChartConsumos1 { get; private set; }
        public Chart BarChartConsumos2 { get; private set; }
        public Chart BarChartConsumos3 { get; private set; }
        public Chart BarChartConsumos4 { get; private set; }
        public Chart ChartOdometros { get; private set; }


        public bool ShowGridFiltros { get; set; }
        public bool ShowGridConsumos { get; set; }
        public bool ShowGridConsumosVapor { get; set; }
        public bool ShowGridConsumosAgua { get; set; }
        public bool ShowGridConsumosCombustible { get; set; }
        public bool ShowGridConsumosElectrico { get; set; }
        public bool ShowGridOdometros { get; set; }
        public bool ShowGridTablas { get; set; }
        #endregion

        #region Contructor
        public HistoricosViewModel(
                                 IHistoricosService historicosService,
                                 INavigationService navigationService,
                                 IUserDialogs userDialogsService,
                                 ITiempoRealService tiempoRealService,
                                 ICatalogosService catalogosService,
                                 IConnectivity connectivity,
                                 IEventAggregator eventAggregator) : base(navigationService, userDialogsService, connectivity,eventAggregator)
        {
            SelectRegionChanged = new DelegateCommand(SelectRegionChangedExecuted);

            Items = new ObservableCollectionExt<ProgressReportModel>();
            _tiempoRealService = tiempoRealService;
            _catalogosService = catalogosService;
            _historicosService = historicosService;

            ShowGridOdometros = true;
            RegionesList = new ObservableCollectionExt<Vertica_Region>();
            TitleToolbar="HISTÓRICOS";

            initPeriodList();

          //  UpdateHistoricos();
        }
        #endregion

        #region Populating    

        private void initPeriodList()
        {
            ListaPeriodos = new ObservableCollection<SfSegmentItem>
            {
                new SfSegmentItem(){ FontColor=Color.FromHex("#597ab0"), Text = "Día", FontSize = 12},
                new SfSegmentItem(){ FontColor=Color.FromHex("#597ab0"), Text = "Mes", FontSize = 12},
                new SfSegmentItem(){ FontColor=Color.FromHex("#597ab0"), Text = "Año", FontSize = 12}
            };
        }

        private async void GetRegiones()
        {
            try
            {
                using (UserDialogsService.Loading("Cargando Regiones...")) {

                    var regiones = await RunSafeApi(_catalogosService.GetRegiones(Profile.Instance.UserName, Profile.Instance.IdClient));

                    RegionesList = new ObservableCollectionExt<Vertica_Region>();
                    foreach (var item in regiones.Response)
                    {


                        Vertica_Region r = new Vertica_Region()
                        {
                            Id = item.Id,
                            Nombre = item.Nombre
                        };

                        RegionesList.Add(r);

                    }
                }
            }
            catch (Exception ex)
            {
                UserDialogsService.Alert("No se encontro informacion para listar las regiones verifique su conexion a internet");
                Debug.WriteLine(ex.Message, TAG);
            }
            RaisePropertyChanged(nameof(RegionesList));
        }

        private async void GetSitios()
        {
            if (SelectedRegion == null) return;
            var idRegion = SelectedRegion.Id;
            try
            {
                using (UserDialogsService.Loading("Cargando Sitios.."))
                {
                    var sitios = await RunSafeApi(_catalogosService.GetSitios(idRegion, Profile.Instance.UserName));

                    SitiosList = new ObservableCollectionExt<Vertica_Sitio>();
                    foreach (var item in sitios.Response)
                    {


                        Vertica_Sitio r = new Vertica_Sitio()
                        {
                            Id = item.Id,
                            Nombre = item.Nombre
                        };

                        SitiosList.Add(r);

                    }
                }
            }
            catch (Exception ex)
            {
                UserDialogsService.Alert("No se encontro informacion para listar los sitios verifique su conexion a internet");
                Debug.WriteLine(ex.Message, TAG);
            }


            RaisePropertyChanged(nameof(SitiosList));

        }

        private async void GetEquipos()
        {
            if (SelectedSitio == null) return;
            var idSitio = SelectedSitio.Id;
            try
            {
                using (UserDialogsService.Loading("Cargando Equipos.."))
                {
                    var equipos = await RunSafeApi(_catalogosService.GetEquipos(idSitio, 8));

                    EquiposList = new ObservableCollectionExt<Vertica_Equipos>();
                    foreach (var item in equipos.Response)
                    {
                        Vertica_Equipos r = new Vertica_Equipos()
                        {
                            Id = item.Id,
                            Nombre = item.Nombre
                        };

                        EquiposList.Add(r);
                    }
                }
            }
            catch (Exception ex)
            {
               Debug.WriteLine(ex.Message, TAG);
                UserDialogsService.Alert("No se encontró información para listar los equipos verifique su conexion a internet");
            }

            RaisePropertyChanged(nameof(EquiposList));
        }

        private async void GetVariables()
        {
            if (SelectedEquipo == null) return;

            UpdateHistoricos();

            var idEquipo = SelectedEquipo.Id;

            var variables = await _catalogosService.GetVariables(idEquipo);

            if (variables != null)
            {
                variables[0].Selected = true;
                VariablesList = new ObservableCollection<Vertica_Tags>(variables);

                RaisePropertyChanged(nameof(VariablesList));
            }
        }

        private void UpdateVariables(Vertica_Tags selectedItem)
        {
            var list =  VariablesList.ToList();
            var lastSelected = list.FindIndex(s => s.Selected == true);
            var currentSelected  = list.FindIndex(s => s.IdTag== selectedItem.IdTag);

            list[lastSelected].Selected = false;
            list[currentSelected].Selected = true;

            VariablesList = new ObservableCollection<Vertica_Tags>(list);

            RaisePropertyChanged(nameof(VariablesList));
        }

        private async  void UpdateHistoricos()
        {
            if (SelectedSitio != null && SelectedEquipo != null)
            {
                using (UserDialogsService.Loading("Cargando Grafica..."))
                {
                    ListHistoricos = new ObservableCollectionExt<Vertica_ConsumoDemandaHistorico>();
                    try
                    {
                        switch (SelectedPeriodo)
                        {
                            case 0:
                                Historicos = await _historicosService.GetHistoricosDia(SelectedSitio.Id, SelectedEquipo.Id, SelectedVariable != null ? SelectedVariable.IdTag.ToString() : "2250063");
                                break;
                            case 1:
                                Historicos = await _historicosService.GetHistoricosMes(SelectedSitio.Id, SelectedEquipo.Id, SelectedVariable != null ? SelectedVariable.IdTag.ToString() : "2250063");
                                break;
                            case 2:
                                Historicos = await _historicosService.GetHistoricosAnio(SelectedSitio.Id, SelectedEquipo.Id, SelectedVariable != null ? SelectedVariable.IdTag.ToString() : "2250063");
                                break;
                        }
                        var data = Historicos.FirstOrDefault();

                        Unidad = data.Unidad;
                        Variable = data.NombreVariable;

                        foreach (var item in data.Puntos)
                        {
                            var xAxisValue = SelectedPeriodo == 0 ? item.Fecha.DayOfWeek.ToString() : (SelectedPeriodo == 1 ? item.Fecha.Month.ToString() : item.Fecha.Year.ToString());
                            ListHistoricos.Add(new Vertica_ConsumoDemandaHistorico() { NoHora = xAxisValue, TagValue = item.Valor });
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.Message, TAG);
                        UserDialogsService.Alert("No se encontró información para actualizar el grafico");
                    }
                }

                RaisePropertyChanged(nameof(ListHistoricos));
            }
        }

        private void ShowFilters()
        {
            ShowGridConsumos = false;
            RaisePropertyChanged(nameof(ShowGridConsumos));

            ShowGridFiltros = true;
            RaisePropertyChanged(nameof(ShowGridFiltros));


            ShowGridOdometros = false;
            RaisePropertyChanged(nameof(ShowGridOdometros));

            ShowGridTablas = false;
            RaisePropertyChanged(nameof(ShowGridTablas));
        }
        #endregion

        #region Commands Methods

        private void SelectRegionChangedExecuted()
        {
            GetSitios();
        }
        
        #endregion

        #region Navigation Params
       
        #endregion

        #region Methods Life Cycle Page
        public override void OnAppearing()
        {
            try
            {
                GetRegiones();
            }
            catch (System.Exception ex)
            {
                Debug.WriteLine(ex.Message, TAG);
            }

        }
        #endregion

    }
}
