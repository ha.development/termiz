﻿using System.Diagnostics;
using Acr.UserDialogs;
using Microcharts;
using Plugin.Connectivity.Abstractions;
using Prism.Events;
using Prism.Navigation;
using SkiaSharp;
using TermizApp.Abstractions;
using TermizApp.Models.Temp;

namespace TermizApp.ViewModels.Home
{
    public class MainPageViewModel : ViewModelBase
    {

        #region Vars
        private static string TAG = nameof(MainPageViewModel);
        #endregion

        #region Vars Commands
        #endregion

        #region Properties
        // valores temporales en lo que se invoca del servicio y basado esto se podran colocar de forma fija o dinamiza sgun corresponda
        private static readonly SKColor Color1 = SKColor.Parse("#af7ac5");
        private static readonly SKColor Color2 = SKColor.Parse("#f5b041");
        private static readonly SKColor Color3 = SKColor.Parse("#e74c3c");
        private static readonly SKColor Color4 = SKColor.Parse("#34495e");
        private static readonly SKColor Color5 = SKColor.Parse("#9c27b0");


        private static readonly SKColor TxtColor = SKColor.Parse("#FFFFFF");

        public Chart EffortChart => new DonutChart()
        {
            Entries = new[]
            {
                new Microcharts.Entry(45)
                {
                    Color = Color1,
                    TextColor =TxtColor,
                    Label = "Up",
                    ValueLabel = $"{45}m",
                },
                new Microcharts.Entry(35)
                {
                    Color = Color2,
                    TextColor = TxtColor,
                    Label = "Down",
                    ValueLabel = $"{35}m",
                },
                 new Microcharts.Entry(20)
                {
                    Color = Color3,
                    TextColor = TxtColor,
                    Label = "Down",
                    ValueLabel = $"{20}m",
                },
      
            },
            BackgroundColor = SKColors.Transparent,
            HoleRadius = 0.5f,
        };

        public ObservableCollectionExt<HomeClassTemp> Items { get; set; }

        #endregion

        #region Contructor
        public MainPageViewModel(INavigationService navigationService,
                                 IUserDialogs userDialogsService,
                                 IConnectivity connectivity,
                                 IEventAggregator eventAggregator) : base(navigationService, userDialogsService, connectivity,eventAggregator)
        {
            TitleToolbar = "DASHBOARD";
            Items = new ObservableCollectionExt<HomeClassTemp>();
            
        }
        #endregion

        #region Populating
        private void PopulateListViewProgress()
        {
            string[] title = new string[] { "Vapor", "Agua", "Combustible", "Eléctico", "Voltaje", "Corriente" };
            string[] titleCount = new string[] { "Kg/h", "m\xB3/h", "I/h", "kw/h"};
            var progress = 62.0;
            var index = 0;
            foreach (var item in title)
            {
                Items.Add(new HomeClassTemp
                {
                    Title = item,
                    Progress = progress,
                    TitleCount = $"{progress} {titleCount[index]}"
                });
                index++;
            }
        }
        #endregion

        #region Commands Methods
        #endregion

        #region Navigation Params
        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            
        }
        #endregion

        #region Methods Life Cycle Page
        public override void OnAppearing()
        {
            try
            {
                if(Items != null && Items.Count == 0)
                {
                    PopulateListViewProgress();
                }
            }
            catch (System.Exception ex)
            {
                Debug.WriteLine(ex.Message, TAG);
            }

        }
        #endregion

    }
}
