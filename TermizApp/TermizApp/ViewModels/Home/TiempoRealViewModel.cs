﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using Acr.UserDialogs;
using Microcharts;
using Plugin.Connectivity.Abstractions;
using Prism.Commands;
using Prism.Events;
using Prism.Navigation;
using SkiaSharp;
using Syncfusion.SfChart.XForms;
using Syncfusion.XForms.Buttons;
using TermizApp.Abstractions;
using TermizApp.LocalData;
using TermizApp.Models.Catalogs;
using TermizApp.Services.Catalogs;
using TermizApp.Views.Popups;
using Xamarin.Forms;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Threading;

namespace TermizApp.ViewModels.Home
{
    public class TiempoRealViewModel : ViewModelBase
    {

        #region Vars
        private int timeDelay;
        private static string TAG = nameof(MainPageViewModel);
        public ObservableCollectionExt<ProgressReportModel> Items { get; set; }
        public List<Vertica_Region> Regiones = new List<Vertica_Region>();
        private readonly IProgressReportService _progressReportService;
        public Microcharts.Entry[] listConsumo1 = new Microcharts.Entry[24];
        public Microcharts.Entry[] listConsumo2= new Microcharts.Entry[24];
        public Microcharts.Entry[] listConsumo3 = new Microcharts.Entry[24];
        public Microcharts.Entry[] listConsumo4 = new Microcharts.Entry[24];
        private ITiempoRealService _tiempoRealService;
        private ICatalogosService _catalogosService;

        public ObservableCollectionExt<Vertica_Region> RegionesList { get; set; }
        public ObservableCollectionExt<Vertica_Sitio> SitiosList { get; set; }
        public ObservableCollectionExt<Vertica_Equipos> EquiposList { get; set; }
        public ObservableCollectionExt<Vertica_Tags> VariablesList { get; set; }
        public ObservableCollectionExt<Vertica_Tablas> TablasList { get; set; }

        public ObservableCollectionExt<Vertica_ConsumoDemandaHoras> ListConsumoVaporHoy { get; set; }
        public ObservableCollectionExt<Vertica_ConsumoDemandaHoras> ListConsumoVaporAyer { get; set; }
        public ObservableCollectionExt<Vertica_ConsumoDemandaHoras> ListConsumoAguaHoy { get; set; }
        public ObservableCollectionExt<Vertica_ConsumoDemandaHoras> ListConsumoCombustibleAyer { get; set; }
        public ObservableCollectionExt<Vertica_ConsumoDemandaHoras> ListConsumoCombustibleHoy { get; set; }
        public ObservableCollectionExt<Vertica_ConsumoDemandaHoras> ListConsumoAguaAyer { get; set; }
        public ObservableCollectionExt<Vertica_ConsumoDemandaHoras> ListConsumoElectricoHoy { get; set; }
        public ObservableCollectionExt<Vertica_ConsumoDemandaHoras> ListConsumoElectricoAyer { get; set; }
        public static CancellationTokenSource CancellationToken { get; set; }
        public ObservableCollectionExt<Options> ConsumosList { get; set; }


        private List<Vertica_ConsumoDemandaHoras> listDemandaVapor = new List<Vertica_ConsumoDemandaHoras>();
        private List<Color> Colors = new List<Color>();

        #endregion

        #region Vars Commands

        public DelegateCommand UpdateConsumosCommand { get; set; }
        public DelegateCommand ShowFiltersCommand { get; set; }
        public DelegateCommand ShowConsumosCommand { get; set; }
        public DelegateCommand ShowOdometrosCommand { get; set; }
        public DelegateCommand ShowTablasCommand { get; set; }
        public DelegateCommand SelectRegionChanged { get; set; }
        public DelegateCommand SelectConsumoCommand { get; set; }
        #endregion

        #region Properties

        private int _heightConsumosVapor;
        public int HeightConsumosVapor
        {
            get { return _heightConsumosVapor; }
            set { SetProperty(ref _heightConsumosVapor, value); }
        }

        private int _heightConsumosAgua;
        public int HeightConsumosAgua
        {
            get { return _heightConsumosAgua; }
            set { SetProperty(ref _heightConsumosAgua, value); }
        }

        private int _heightConsumosCombustible;
        public int HeightConsumosCombustible
        {
            get { return _heightConsumosCombustible; }
            set { SetProperty(ref _heightConsumosCombustible, value); }
        }

        private string _message;
        public string Message
        {
            get { return _message; }
            set { SetProperty(ref _message, value); }
        }

        private string _ejex;
        public string Ejex
        {
            get { return _ejex; }
            set { SetProperty(ref _ejex, value); }
        }

        private string _periodoActual;
        public string PeriodoActual
        {
            get { return _periodoActual; }
            set { SetProperty(ref _periodoActual, value); }
        }

        private string _periodoAnterior;
        public string PeriodoAnterior
        {
            get { return _periodoAnterior; }
            set { SetProperty(ref _periodoAnterior, value); }
        }

        private ObservableCollection<SfSegmentItem> _listaPeriodos = new ObservableCollection<SfSegmentItem>();
        public ObservableCollection<SfSegmentItem> ListaPeriodos
        {
            get { return _listaPeriodos; }
            set { _listaPeriodos = value; }
        }

        private int _selectedPeriodo;
        public int SelectedPeriodo
        {
            get { return _selectedPeriodo; }
            set
            {
                SetProperty(ref _selectedPeriodo, value);
                SelectedPeriodoRaise();
            }
        }

        private Options _selectedItem;
        public Options SelectedItem
        {
            get
            {
                return _selectedItem;
            }
            set
            {
                _selectedItem = value;

                if (_selectedItem == null)
                    return;

                SelectGraficaConsumo(_selectedItem.Id);


                //SelectedItem = null;
            }
        }

        private Vertica_Tags _selectedVariable;
        public Vertica_Tags SelectedVariable
        {
            get
            {
                return _selectedVariable;
            }
            set
            {
                _selectedVariable = value;

                if (_selectedVariable == null)
                    return;

                SelectOpcionVariable(_selectedVariable.IdTag);
            }
        }

        private Vertica_Region _selectedRegion;
        public Vertica_Region SelectedRegion
        {
            get { return _selectedRegion; }
            set
            {
                SetProperty(ref _selectedRegion, value);
                GetSitios();
            }
        }

        private Vertica_Sitio _selectedSitio;
        public Vertica_Sitio SelectedSitio
        {
            get { return _selectedSitio; }
            set
            {
                SetProperty(ref _selectedSitio, value);
                GetEquipos();
            }
        }


        private Vertica_Equipos _selectedEquipo;
        public Vertica_Equipos SelectedEquipo
        {
            get { return _selectedEquipo; }
            set
            {
                SetProperty(ref _selectedEquipo, value);

                GetVariables();
                //CargaOpciones();
                //SelectGraficaConsumo(1);                
                GetTablas();
                SelectedItem = ConsumosList[0];
                if (_selectedVariable == null)
                    return;
                SelectOpcionVariable(_selectedVariable.IdTag);
            }
        }

        private String _labelVariable="";
        public String labelVariable
        {
            get { return _labelVariable; }
            set
            {
                SetProperty(ref _labelVariable, value);

            }
        }


        private String _labelUnidad = "";
        public String labelUnidad
        {
            get { return _labelUnidad; }
            set
            {
                SetProperty(ref _labelUnidad, value);

            }
        }

        private String _labelValor = "";
        public String labelValor
        {
            get { return _labelValor; }
            set
            {
                SetProperty(ref _labelValor, value);

            }
        }

        private string _startRangeColor;
        public string StartRangeColor
        {
            get { return _startRangeColor; }
            set
            {
                SetProperty(ref _startRangeColor, value);

            }
        }

        private string _acceptableRangeColor;
        public string AcceptableRangeColor
        {
            get { return _acceptableRangeColor; }
            set
            {
                SetProperty(ref _acceptableRangeColor, value);

            }
        }

        private string _endRangeColor;
        public string EndRangeColor
        {
            get { return _endRangeColor; }
            set
            {
                SetProperty(ref _endRangeColor, value);

            }
        }

        private float _StartValueOdometro = 0f;
        public float StartValueOdometro
        {
            get { return _StartValueOdometro; }
            set
            {
                SetProperty(ref _StartValueOdometro, value);

            }
        }

        private float _FirstRangeOdometro = 0f;
        public float FirstRangeOdometro
        {
            get { return _FirstRangeOdometro; }
            set
            {
                SetProperty(ref _FirstRangeOdometro, value);

            }
        }

        private float _SecondRangeOdometro = 0f;
        public float SecondRangeOdometro
        {
            get { return _SecondRangeOdometro; }
            set
            {
                SetProperty(ref _SecondRangeOdometro, value);

            }
        }


        private float _EndValueOdometro = 0f;
        public float EndValueOdometro
        {
            get { return _EndValueOdometro; }
            set
            {
                SetProperty(ref _EndValueOdometro, value);

            }
        }

        private float _ValueOdometro = 0f;
        public float ValueOdometro
        {
            get { return _ValueOdometro; }
            set
            {
                SetProperty(ref _ValueOdometro, value);

            }
        }


        public bool ShowGridFiltros { get; set; }
        public bool ShowGridConsumos { get; set; }
        public bool ShowGridConsumosVapor { get; set; }
        public bool ShowGridConsumosAgua { get; set; }
        public bool ShowGridConsumosCombustible { get; set; }
        public bool ShowGridConsumosElectrico { get; set; }
        public bool ShowGridOdometros { get; set; }
        public bool ShowGridTablas { get; set; }


        #endregion

        #region Contructor
        public TiempoRealViewModel(IProgressReportService progressReportService,
                                 INavigationService navigationService,
                                 IUserDialogs userDialogsService,
                                 ITiempoRealService tiempoRealService,
                                 ICatalogosService catalogosService,
                                 IConnectivity connectivity,
                                 IEventAggregator eventAggregator) : base(navigationService, userDialogsService, connectivity, eventAggregator)
        {
            _progressReportService = progressReportService;
            UpdateConsumosCommand = new DelegateCommand(OnUpdateConsumosCommandExecuted);
            ShowFiltersCommand = new DelegateCommand(ShowFiltersCommandExecuted);
            ShowConsumosCommand = new DelegateCommand(ShowConsumosCommandExecuted);
            ShowOdometrosCommand = new DelegateCommand(ShowOdometrosCommandExecuted);
            ShowTablasCommand = new DelegateCommand(ShowTablasCommandExecuted);
            SelectRegionChanged = new DelegateCommand(SelectRegionChangedExecuted);
            SelectConsumoCommand = new DelegateCommand(SelectConsumoCommandExecuted);

            Items = new ObservableCollectionExt<ProgressReportModel>();
            _tiempoRealService = tiempoRealService;
            _catalogosService = catalogosService;
            ShowGridFiltros = true;
            ShowGridConsumos = false;
            ShowGridConsumosVapor = true;
            ShowGridConsumosAgua = false;
            ShowGridConsumosCombustible = false;
            ShowGridConsumosElectrico = false;
            ShowGridOdometros = false;
            ShowGridTablas = false;
            RegionesList = new ObservableCollectionExt<Vertica_Region>();
            TitleToolbar="TIEMPO REAL";

            //await UpdateDemandaVapor();
            initPeriodList();
            CargaOpciones();
            InitTimeDelay();

            this.HeightConsumosAgua = -1;
            this.HeightConsumosVapor = -1;
            this.HeightConsumosCombustible = -1;
        }

       
        #endregion

        #region Populating

        private async void SelectedPeriodoRaise()
        {
            if (SelectedEquipo == null) return;

            int IdTag = 0;
            if (SelectedItem != null) IdTag = SelectedItem.Id;

            switch (SelectedPeriodo)
            {
                case 0:
                    this.PeriodoActual = "Hoy";
                    this.PeriodoAnterior = "Ayer";
                    this.Ejex = "Horas";
                    this.Message = "Últimas 24 hrs";
                    await UpdateDemandaVapor(IdTag, "d");
                    break;
                case 1:
                    this.PeriodoActual = "Mes Actual";
                    this.PeriodoAnterior = "Mes Anterior";
                    this.Ejex = "Días";
                    this.Message = "Últimos 30 días";
                    await UpdateDemandaVapor(IdTag, "m");
                    break;
                case 2:
                    this.PeriodoActual = "Año Actual";
                    this.PeriodoAnterior = "Año Anterior";
                    this.Ejex = "Meses";
                    this.Message = "Últimos 12 meses";
                    await UpdateDemandaVapor(IdTag, "a");
                    break;                
            }
            RaisePropertyChanged(nameof(PeriodoActual));
            RaisePropertyChanged(nameof(PeriodoAnterior));
            RaisePropertyChanged(nameof(Ejex));
            RaisePropertyChanged(nameof(Message));
        }

        private void initPeriodList()
        {
            ListaPeriodos = new ObservableCollection<SfSegmentItem>
            {
                new SfSegmentItem(){ FontColor=Color.FromHex("#597ab0"), Text = "Día", FontSize = 12},
                new SfSegmentItem(){ FontColor=Color.FromHex("#597ab0"), Text = "Mes", FontSize = 12},
                new SfSegmentItem(){ FontColor=Color.FromHex("#597ab0"), Text = "Año", FontSize = 12}
            };
        }

        private async void InitTimeDelay()
        {
            var result = await RunSafeApi(_tiempoRealService.GetTiempoActualizacionOdometros());

            if (result.Status == TypeReponse.Ok)
            {
                timeDelay = result.Response;
            }
        }


        private async void CargaOpciones()
        {
            try
            {
                ConsumosList = new ObservableCollectionExt<Options>();

                var variablesConsumosList = await RunSafeApi(_tiempoRealService.GetVariablesTiempoRealMovil());
                int i = 1;
                foreach (var item in variablesConsumosList.Response)
                {
                    ConsumosList.Add(new Options { Id = i++, Clave = item.Clave, Nombre = item.Variable, Selected = false, Imagen = "users.png" });
                }                
                ShowGridConsumosVapor = true;
                ShowGridConsumosAgua = false;
                ShowGridConsumosCombustible = false;
                ShowGridConsumosElectrico = false;

                RaisePropertyChanged(nameof(ConsumosList));

                RaisePropertyChanged(nameof(ShowGridConsumosVapor));
            }
            catch
            {
                return;
            }            
        }

        private void UpdateOpciones(int i)
        {              
            var list = ConsumosList.ToList();
            ConsumosList = new ObservableCollectionExt<Options>();

            for(int index = 0; index < list.Count; index++)
            {
                if ((index + 1) == i)
                {
                    ConsumosList.Add(new Options { Id = list[index].Id, Clave = list[index].Clave, Nombre = list[index].Nombre, Selected = true, Imagen = "" });
                }
                else
                {
                    ConsumosList.Add(new Options { Id = list[index].Id, Clave = list[index].Clave, Nombre = list[index].Nombre, Selected = false, Imagen = "" });
                }
            }
            RaisePropertyChanged(nameof(ConsumosList));

            ShowGraficaConsumo(i);
        }

        private async void UpdateOpcionesVariable(int idTag)
        {

            if (SelectedEquipo == null) return;

            var list = new ObservableCollectionExt<Vertica_Tags>();

            try
            {
                await Task.Run( async () => PopulateRealTimeOdometer(idTag, 20));
            }
            catch (Exception)
            {
                resetGaugeGraph();
                UserDialogsService.Alert("No se encontró información para listar las regiones verifique su conexion a internet");
            }


            foreach (var item in VariablesList)
            {
                var selected_option = false;

                if (item.IdTag == idTag)
                {
                    selected_option = true;


                }
                    Vertica_Tags r = new Vertica_Tags()
                    {
                        IdTag = item.IdTag,
                        Tag = item.Tag,
                        Selected = selected_option
                    };
                list.Add(r);

            }

            VariablesList = new ObservableCollectionExt<Vertica_Tags>();
            VariablesList = list;
            RaisePropertyChanged(nameof(VariablesList));


        }
        private async Task PopulateRealTimeOdometer(int idTag, int timer = 20)
        {
            var idEquipo = SelectedEquipo.Id;
            bool firstime = true;

            if (App.CancellationToken != null) App.CancellationToken.Cancel();

            App.CancellationToken = new CancellationTokenSource();
            

            while (!App.CancellationToken.IsCancellationRequested)
            {
                try
                {
                    int delay = this.timeDelay * 1000;
                    if (firstime)
                    {
                        delay = 0;
                        firstime = false;
                    }
                    
                    App.CancellationToken.Token.ThrowIfCancellationRequested();
                    await Task.Delay(delay, App.CancellationToken.Token).ContinueWith(async (arg) => {                        
                        if (!App.CancellationToken.Token.IsCancellationRequested)
                        {
                            App.CancellationToken.Token.ThrowIfCancellationRequested();

                            UserDialogsService.ShowLoading("Cargando datos...");
                            var odometro = await RunSafeApi(_tiempoRealService.GetOdometros(idEquipo, SelectedVariable.IdTag.ToString())); //""2250064"4500009" - SelectedVariable.IdTag.ToString()

                            if (odometro.Response.Count > 0)
                            {
                                _labelVariable = odometro.Response[0].Tag;
                                _labelValor = odometro.Response[0].TagValue.ToString();
                                _labelUnidad = odometro.Response[0].UnidadMedida.ToString();
                                StartRangeColor = odometro.Response[0].ColorRangoMenorValorObjetivo;
                                AcceptableRangeColor = odometro.Response[0].Color;
                                EndRangeColor = odometro.Response[0].ColorRangoMayorValorObjetivo;
                                ValueOdometro = odometro.Response[0].TagValue;
                                StartValueOdometro = odometro.Response[0].MinValue;
                                FirstRangeOdometro = odometro.Response[0].TagValue - odometro.Response[0].TagValue * odometro.Response[0].PorcetajeDesviacion / 100;
                                SecondRangeOdometro = odometro.Response[0].TagValue + odometro.Response[0].TagValue * odometro.Response[0].PorcetajeDesviacion / 100;
                                _EndValueOdometro = odometro.Response[0].MaxValue;

                                UserDialogsService.HideLoading();
                                if (StartValueOdometro == 0 && _EndValueOdometro == 0)
                                {
                                    UserDialogsService.HideLoading();
                                    resetGaugeGraph();
                                    App.CancellationToken.Cancel();
                                    UserDialogsService.Alert(string.Format("Valor mínimo: {0}, Valor máximo: {1}", StartValueOdometro, _EndValueOdometro), "Datos no válidos", "Aceptar");
                                }
                            }
                            else
                            {
                                if (App.CancellationToken != null)
                                {
                                    UserDialogsService.HideLoading();
                                    UserDialogsService.Alert("No se encontró información para atualizar en gráfico");
                                    resetGaugeGraph();
                                    App.CancellationToken.Cancel();
                                }
                            }

                            RaisePropertyChanged(nameof(labelVariable));
                            RaisePropertyChanged(nameof(labelUnidad));
                            RaisePropertyChanged(nameof(labelValor));
                            RaisePropertyChanged(nameof(ValueOdometro));
                            RaisePropertyChanged(nameof(StartRangeColor));
                            RaisePropertyChanged(nameof(AcceptableRangeColor));
                            RaisePropertyChanged(nameof(EndRangeColor));
                            RaisePropertyChanged(nameof(StartValueOdometro));
                            RaisePropertyChanged(nameof(FirstRangeOdometro));
                            RaisePropertyChanged(nameof(SecondRangeOdometro));
                            RaisePropertyChanged(nameof(EndValueOdometro));
                        }
                    });

                    //if (StartValueOdometro == 0 && _EndValueOdometro == 0)
                    //{
                    //    UserDialogsService.Alert(string.Format("Valor mínimo: {0}, Valor máximo: {1}, Límite mínimo: {2}, Límite máximo: {3}. ", StartValueOdometro, _EndValueOdometro, odometro.Response[0].LimiteMin, odometro.Response[0].LimiteMax), "Datos no válidos", "Aceptar");
                    //}
                    

                }
                catch (Exception ex)
                {
                    UserDialogsService.HideLoading();
                    if (App.CancellationToken != null)
                    {
                        App.CancellationToken.Cancel();
                    }
                    Debug.WriteLine("EX 1: " + ex.Message);
                }
            }
        }

        private void resetGaugeGraph()
        {
            _labelVariable = "Selecione Una Variable";
            _labelUnidad = "Unidad de la Variable";
            _labelValor = "0";
            ValueOdometro = 50f;
            StartValueOdometro = 0f;
            FirstRangeOdometro = 40f;
            SecondRangeOdometro = 60f;
            _EndValueOdometro = 100f;
        }

        private void ShowGraficaConsumo(int id)
        {
            ShowGridConsumosVapor = false;
            ShowGridConsumosAgua = false;
            ShowGridConsumosCombustible = false;
            ShowGridConsumosElectrico = false;

            this.HeightConsumosAgua = 0;
            this.HeightConsumosVapor = 0;
            this.HeightConsumosCombustible = 0;

            if (id == 1)
            {
                switch (Device.RuntimePlatform)
                {
                    case Device.iOS:
                        this.HeightConsumosVapor = -1;
                        ShowGridConsumosVapor = true;
                        break;
                    default:
                        ShowGridConsumosVapor = true;
                        break;
                }                
            }
            if (id == 2)
            {
                switch (Device.RuntimePlatform)
                {
                    case Device.iOS:
                        this.HeightConsumosAgua = -1;
                        ShowGridConsumosAgua = true;
                        break;
                    default:
                        ShowGridConsumosAgua = true;
                        break;
                }                
            }
            if (id == 3)
            {
                switch (Device.RuntimePlatform)
                {
                    case Device.iOS:
                        this.HeightConsumosCombustible = -1;
                        ShowGridConsumosCombustible = true;
                        break;
                    default:
                        ShowGridConsumosCombustible = true;
                        break;
                }
                
            }
            if (id == 4) ShowGridConsumosElectrico = true;

            RaisePropertyChanged(nameof(ShowGridConsumosVapor));
            RaisePropertyChanged(nameof(ShowGridConsumosAgua));
            RaisePropertyChanged(nameof(ShowGridConsumosCombustible));
            RaisePropertyChanged(nameof(ShowGridConsumosElectrico));
        }

        private async void SelectGraficaConsumo(int id)
        {
            UpdateOpciones(id);

            switch (SelectedPeriodo)
            {
                case 0:
                    this.PeriodoActual = "Hoy";
                    this.PeriodoAnterior = "Ayer";
                    this.Ejex = "Horas";
                    this.Message = "Últimas 24 hrs";
                    await UpdateDemandaVapor(id, "d");
                    break;
                case 1:
                    this.PeriodoActual = "Mes Actual";
                    this.PeriodoAnterior = "Mes Anterior";
                    this.Ejex = "Días";
                    this.Message = "Últimos 30 dias";
                    await UpdateDemandaVapor(id, "m");
                    break;
                case 2:
                    this.PeriodoActual = "Año Actual";
                    this.PeriodoAnterior = "Año Anterior";
                    this.Ejex = "Meses";
                    this.Message = "Últimos 12 meses";
                    await UpdateDemandaVapor(id, "a");
                    break;             
            }
            RaisePropertyChanged(nameof(PeriodoActual));
            RaisePropertyChanged(nameof(PeriodoAnterior));
            RaisePropertyChanged(nameof(Ejex));
            RaisePropertyChanged(nameof(Message));
        }


      private void SelectOpcionVariable(int id)
        {
            UpdateOpcionesVariable(id);
        }

        private async void GetRegiones()
        {
            try
            {
                using (UserDialogsService.Loading("Cargando Regiones"))
                {
                    var regiones = await RunSafeApi(_catalogosService.GetRegiones(Profile.Instance.UserName, Profile.Instance.IdClient));

                    RegionesList = new ObservableCollectionExt<Vertica_Region>();
                    foreach (var item in regiones.Response)
                    {
                        Vertica_Region r = new Vertica_Region()
                        {
                            Id = item.Id,
                            Nombre = item.Nombre
                        };

                        RegionesList.Add(r);

                    }

                    RaisePropertyChanged(nameof(RegionesList));
                }
            }
            catch
            {

            }           
        }

        private async void GetSitios()
        {
            if (SelectedRegion == null) return;

            try
            {
                using (UserDialogsService.Loading("Cargando Sitios"))
                {
                    var idRegion = SelectedRegion.Id;
                    var sitios = await RunSafeApi(_catalogosService.GetSitios(idRegion, Profile.Instance.UserName));

                    SitiosList = new ObservableCollectionExt<Vertica_Sitio>();
                    foreach (var item in sitios.Response)
                    {
                        Vertica_Sitio r = new Vertica_Sitio()
                        {
                            Id = item.Id,
                            Nombre = item.Nombre
                        };

                        SitiosList.Add(r);

                    }

                    RaisePropertyChanged(nameof(SitiosList));
                }
            }
            catch
            {

            }
            
        }

        private async void GetEquipos()
        {
            if (SelectedSitio == null) return;

            try
            {
                using (UserDialogsService.Loading("Cargando Equipos"))
                {
                    var idSitio = SelectedSitio.Id;

                    var equipos = await RunSafeApi(_catalogosService.GetEquipos(idSitio, 11));

                    EquiposList = new ObservableCollectionExt<Vertica_Equipos>();
                    foreach (var item in equipos.Response)
                    {


                        Vertica_Equipos r = new Vertica_Equipos()
                        {
                            Id = item.Id,
                            Nombre = item.Nombre
                        };

                        EquiposList.Add(r);

                    }
                    RaisePropertyChanged(nameof(EquiposList));
                }
            }
            catch
            {

            }
        }

        private async void GetVariables()
        {
            if (SelectedEquipo == null) return;

            var idEquipo = SelectedEquipo.Id;

            var variables = await RunSafeApi(_catalogosService.GetVariables(idEquipo));
            int i=0;
            VariablesList = new ObservableCollectionExt<Vertica_Tags>();
            foreach (var item in variables.Response)
            {

           var selected_option =false;
                if(i==0) selected_option=true;
                i++;
                Vertica_Tags r = new Vertica_Tags()
                {
                    IdTag = item.IdTag,
                    Tag = item.Tag,
                    Selected = selected_option
                };

                VariablesList.Add(r);
           
            }
            RaisePropertyChanged(nameof(VariablesList));

        }

        private async void GetTablas()
        {
            if (SelectedEquipo == null) return;

            var idEquipo = SelectedEquipo.Id;
            var idSitio = SelectedSitio.Id;

            var tablas = await RunSafeApi(_tiempoRealService.GetTablas(idSitio,idEquipo, Profile.Instance.IsAdmin));

            TablasList = new ObservableCollectionExt<Vertica_Tablas>();
            foreach (var item in tablas.Response)
            {


                Vertica_Tablas r = new Vertica_Tablas()
                {
                    IdTag = item.IdTag,
                    Tag = item.Tag,
                     Taxonomia = item.Taxonomia,
                     TagValue = item.TagValue,
                     UnidadMedida = item.UnidadMedida

                };

                TablasList.Add(r);

            }

            RaisePropertyChanged(nameof(TablasList));

        }

        private async void UpdateVariables(int i)
        {
            var variables = await RunSafeApi(_catalogosService.GetVariables(1));

            VariablesList = new ObservableCollectionExt<Vertica_Tags>();
            foreach (var item in variables.Response)
            {
                var selected_option =false;
                if(i==item.IdTag) selected_option=true;
                Vertica_Tags r = new Vertica_Tags()
                {
                    IdTag = item.IdTag,
                    Tag = item.Tag,
                    Selected = selected_option
                };
                VariablesList.Add(r);
           
            }

            RaisePropertyChanged(nameof(VariablesList));
        }

        private async Task UpdateDemandaVapor(int id =0, string period = "d")
        {
            ListConsumoVaporHoy = new ObservableCollectionExt<Vertica_ConsumoDemandaHoras>();
            ListConsumoVaporAyer = new ObservableCollectionExt<Vertica_ConsumoDemandaHoras>();

            ListConsumoAguaHoy = new ObservableCollectionExt<Vertica_ConsumoDemandaHoras>();
            ListConsumoAguaAyer = new ObservableCollectionExt<Vertica_ConsumoDemandaHoras>();

            ListConsumoCombustibleHoy = new ObservableCollectionExt<Vertica_ConsumoDemandaHoras>();
            ListConsumoCombustibleAyer = new ObservableCollectionExt<Vertica_ConsumoDemandaHoras>();            

            RaisePropertyChanged(nameof(ListConsumoVaporHoy));
            RaisePropertyChanged(nameof(ListConsumoVaporAyer));

            RaisePropertyChanged(nameof(ListConsumoAguaHoy));
            RaisePropertyChanged(nameof(ListConsumoAguaAyer));

            RaisePropertyChanged(nameof(ListConsumoCombustibleHoy));
            RaisePropertyChanged(nameof(ListConsumoCombustibleAyer));

            if (SelectedEquipo == null)
            {
                return;
            }

            var idEquipo = SelectedEquipo.Id;

            if (id != 0)
            {
                using (UserDialogsService.Loading("Actualizando datos del Gráfico"))
                {
                    ResponseBase<Vertica_ConsumoDemandaTiempoReal> result = new ResponseBase<Vertica_ConsumoDemandaTiempoReal>();
                    switch (SelectedItem.Clave)
                    {
                        case "TAG00000051":
                            result = await RunSafeApi(_tiempoRealService.GetConsumoTiempoRealPorPeriodo(idEquipo, 0, SelectedRegion.Id, SelectedSitio.Id, SelectedItem.Clave, Profile.Instance.IdClient, period));

                            for(int i = 0; i < result.Response.Actual.Length; i++)
                            {
                                ListConsumoVaporHoy.Add(new Vertica_ConsumoDemandaHoras() { NoHora = i+1, TagValue = result.Response.Actual[i] });
                                ListConsumoVaporAyer.Add(new Vertica_ConsumoDemandaHoras() { NoHora = i+1, TagValue = result.Response.Anterior[i] });
                            }                     
                            RaisePropertyChanged(nameof(ListConsumoVaporHoy));
                            RaisePropertyChanged(nameof(ListConsumoVaporAyer));
                            break;
                        case "TAG00000053":
                            result = await RunSafeApi(_tiempoRealService.GetConsumoTiempoRealPorPeriodo(idEquipo, 0, SelectedRegion.Id, SelectedSitio.Id, SelectedItem.Clave, Profile.Instance.IdClient, period));
                            for (int i = 0; i < result.Response.Actual.Length; i++)
                            {
                                ListConsumoAguaHoy.Add(new Vertica_ConsumoDemandaHoras() { NoHora = i + 1, TagValue = result.Response.Actual[i] });
                                ListConsumoAguaAyer.Add(new Vertica_ConsumoDemandaHoras() { NoHora = i + 1, TagValue = result.Response.Anterior[i] });
                            }
                            
                            RaisePropertyChanged(nameof(ListConsumoAguaHoy));
                            RaisePropertyChanged(nameof(ListConsumoAguaAyer));
                            break;
                        case "TAG00000052":
                            result = await RunSafeApi(_tiempoRealService.GetConsumoTiempoRealPorPeriodo(idEquipo, 0, SelectedRegion.Id, SelectedSitio.Id, SelectedItem.Clave, Profile.Instance.IdClient, period));
                            for (int i = 0; i < result.Response.Actual.Length; i++)
                            {
                                ListConsumoCombustibleHoy.Add(new Vertica_ConsumoDemandaHoras() { NoHora = i + 1, TagValue = result.Response.Actual[i] });
                                ListConsumoCombustibleAyer.Add(new Vertica_ConsumoDemandaHoras() { NoHora = i + 1, TagValue = result.Response.Anterior[i] });
                            }
                            
                            RaisePropertyChanged(nameof(ListConsumoCombustibleHoy));
                            RaisePropertyChanged(nameof(ListConsumoCombustibleAyer));
                            break;                        
                    }
                }            

                //var consumos = await RunSafeApi(_tiempoRealService.GetConsumoTiempoReal(SelectedEquipo.Id, 9750055, SelectedRegion.Id, SelectedSitio.Id, "TAG00000029", Profile.Instance.IdClient, "a"));       

                //listDemandaVapor = consumos.Response;
             
                ShowGridConsumos = false;
                RaisePropertyChanged(nameof(ShowGridConsumos));

                ShowGridConsumosVapor = true;
                RaisePropertyChanged(nameof(ShowGridConsumosVapor));

                ShowGridFiltros = false;
                RaisePropertyChanged(nameof(ShowGridFiltros));
            }            
        }

        private  void UpdateOdometros()
        {

            ShowOdometros();
        }

        private void ShowFilters()
        {
            ShowGridConsumos = false;
            RaisePropertyChanged(nameof(ShowGridConsumos));

            ShowGridFiltros = true;
            RaisePropertyChanged(nameof(ShowGridFiltros));


            ShowGridOdometros = false;
            RaisePropertyChanged(nameof(ShowGridOdometros));

            ShowGridTablas = false;
            RaisePropertyChanged(nameof(ShowGridTablas));


        }

        private void ShowConsumos()
        {
            ShowGridConsumos = true;
            RaisePropertyChanged(nameof(ShowGridConsumos));

            ShowGridFiltros = false;
            RaisePropertyChanged(nameof(ShowGridFiltros));


            ShowGridOdometros = false;
            RaisePropertyChanged(nameof(ShowGridOdometros));

            ShowGridTablas = false;
            RaisePropertyChanged(nameof(ShowGridTablas));
        }

        private void ShowOdometros()
        {
            ShowGridConsumos = false;
            RaisePropertyChanged(nameof(ShowGridConsumos));

            ShowGridFiltros = false;
            RaisePropertyChanged(nameof(ShowGridFiltros));


            ShowGridOdometros = true;
            RaisePropertyChanged(nameof(ShowGridOdometros));

            ShowGridTablas = false;
            RaisePropertyChanged(nameof(ShowGridTablas));

        }

        private void ShowTablas()
        {
            ShowGridConsumos = false;
            RaisePropertyChanged(nameof(ShowGridConsumos));

            ShowGridFiltros = false;
            RaisePropertyChanged(nameof(ShowGridFiltros));


            ShowGridOdometros = false;
            RaisePropertyChanged(nameof(ShowGridOdometros));

            ShowGridTablas = true;
            RaisePropertyChanged(nameof(ShowGridTablas));


        }

        #endregion

        #region Commands Methods        

        private void SelectRegionChangedExecuted()
        {
            GetSitios();
        }

        private void SelectConsumoCommandExecuted()
        {
            try
            {
                CargaOpciones();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message, TAG);
            }
        }

        private void ShowTablasCommandExecuted()
        {
            try
            {
                ShowTablas();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message, TAG);
            }
        }

        private void ShowFiltersCommandExecuted()
        {
            try
            {
                ShowFilters();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message, TAG);
            }
        }

        private void ShowConsumosCommandExecuted()
        {
            try
            {
                ShowConsumos();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message, TAG);
            }
        }

        private void ShowOdometrosCommandExecuted()
        {
            try
            {
                ShowOdometros();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message, TAG);
            }
        }

        private async void OnUpdateConsumosCommandExecuted()
        {
            try
            {
                await UpdateDemandaVapor();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message, TAG);
            }
        }

        #endregion

        #region Navigation Params
       
        #endregion

        #region Methods Life Cycle Page
        public override void OnAppearing()
        {
            try
            {

                GetRegiones();
                //GetSitios();
                //GetEquipos();
                //GetVariables();

                //CargaOpciones();
                if (SelectedVariable != null)
                {
                    Task.Run(() => PopulateRealTimeOdometer(SelectedVariable.IdTag, 20));
                }
            }
            catch (System.Exception ex)
            {
                Debug.WriteLine(ex.Message, TAG);
            }

        }


        
        public override void OnDisappearing()
        {
            // Handle when your app sleeps
            if (App.CancellationToken != null)
            {
                App.CancellationToken.Cancel();
            }
        }

        public override void Destroy()
        {
            // Handle when your app sleeps
            if (App.CancellationToken != null)
            {
                App.CancellationToken.Cancel();
                //App.CancellationToken = null;
            }
        }
        #endregion

    }
}
