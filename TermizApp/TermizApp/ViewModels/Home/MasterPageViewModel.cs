﻿using System;
using System.Diagnostics;
using System.Linq;
using Acr.UserDialogs;
using Plugin.Connectivity.Abstractions;
using Prism.Commands;
using Prism.Events;
using Prism.Navigation;
using TermizApp.Abstractions;
using TermizApp.Events;
using TermizApp.LocalData;
using TermizApp.Models.Catalogs;
using TermizApp.Services.Session;
using TermizApp.Settings;
using TermizApp.Views.EfficiencyBoards;
using Xamarin.Forms;

namespace TermizApp.ViewModels.Home
{
    public class MasterPageViewModel : ViewModelBase
    {

        #region Vars
        private static readonly string TAG = nameof(MasterPageViewModel);
        private string currentPage = "StartCenter";
        private readonly ISessionService services;
        #endregion

        #region Vars Commands
        public DelegateCommand OnSelectItemCommand { get; set; }
        #endregion

        #region Properties
        public ObservableCollectionExt<UserModel> Clients { get; set; }
        private ObservableCollectionExt<Menu> itemsMenu;
        public ObservableCollectionExt<Menu> ItemsMenu
        {
            get { return itemsMenu; }
            set
            {
                SetProperty(ref itemsMenu, value);
            }

        }
        public Menu selectItem;
        public Menu SelectItem
        {
            get { return selectItem; }
            set
            {
                SetProperty(ref selectItem, value);
            }
        }
        private string username;
        public string Username
        {
            get { return username; }
            set
            {
                SetProperty(ref username, value);
            }
        }
        private bool isAdmin;
        public bool IsAdmin
        {
            get => isAdmin;
            set => SetProperty(ref isAdmin, value);
        }
        private UserModel client;
        public UserModel Client
        {
            get => client;
            set
            {
                SetProperty(ref client, value);
                if(Client != null)
                {
                    ChangeClient();
                }
            }
        }
        private string urlIconLogo;
        public string UrlIconLogo
        {
            get => urlIconLogo;
            set => SetProperty(ref urlIconLogo, value);
        }
        #endregion

        #region Constructor
        public MasterPageViewModel(ISessionService sessionService,
                                   INavigationService navigationService,
                                   IUserDialogs userDialogsService, 
                                   IConnectivity connectivity,
                                   IEventAggregator eventAggregator) : base(navigationService, userDialogsService, connectivity, eventAggregator)
        {
            services = sessionService;
            Clients = new ObservableCollectionExt<UserModel>();
            Username = Profile.Instance.Name;
            CreatedMenu();
            OnSelectItemCommand = new DelegateCommand(OnSelectItemCommandExecuted);
            CloseSessionCommand = new DelegateCommand(CloseSessionCommandExecuted);
            IsAdmin = Profile.Instance.IsAdmin;
            PopulatePicker();
            //if (IsAdmin) PopulatePicker();
        }
        #endregion

        #region Methods
        private void CreatedMenu()
        {
            ItemsMenu = new ObservableCollectionExt<Menu>()
            {
                new Menu{ Page= nameof(EfficiencyBoardsMenuPage), Title="Tablero de Eficiencia", Icon="TableroEficiencia.png" , ChangeMaster = true},
                new Menu{ Page= "Home", Title="Tablero de Seguridad", Icon="DashboardOperativoB.png"},
                new Menu{ Page= "Home", Title="Tablero de Continuidad", Icon="DashboardTecnicoB.png"},
                new Menu{ Page= "TiempoReal", Title="Tablero de Tiempo Real", Icon="CalderasB.png"},
                new Menu{ Page= "Historicos", Title="Tablero Histórico", Icon="HistoricosIconB.png"},
                new Menu{ Page= "Home", Title="Analiticos Gráficas", Icon="AnaliticosGraficasB.png"},
                new Menu{ Page= "Home", Title="Tableros Dinámicos", Icon="TablerosDinamicosB.png"}
            };
        }
        #endregion

        #region Methods Populating
        private async void PopulatePicker()
        {
            try
            {
                var result = await RunSafeApi(services.GetClients());
                if (result.Status == TypeReponse.Ok && result.Response != null)
                {
                    if (IsAdmin)
                    {
                        Clients.Reset(result.Response.OrderBy(x => x.Nombre).ToList());
                        Client = Clients[0];
                        return;
                    }

                    Clients.Reset(result.Response.Where(x => x.IdCliente == Profile.Instance.IdClient).ToList());
                    Client = Clients[0];
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message,TAG);
            }
        }

        private async void ChangeClient()
        {
            Profile.Instance.IdClient = Client.Id;
            var result = await RunSafeApi(services.GetFile(Client.IconoLogo));
            if(result.Status == TypeReponse.Ok && result.Response != null)
            {
                UrlIconLogo = AppConfiguration.Values.BaseUrl + $"/{result.Response.File_Id}/{result.Response.Description}";
                Events.GetEvent<ChangeOfClientEvent>().Publish(Client.Id);
            }
        }
        #endregion

        #region Commands Methods
        private async void OnSelectItemCommandExecuted()
        {
            try
            {
                if (SelectItem != null)
                {
                    if(SelectItem.ChangeMaster)
                    {
                        await NavigationService.NavigateAsync(new Uri($"{currentPage}/{SelectItem.Page}", UriKind.Relative));
                    }
                    else
                    {
                        currentPage = SelectItem.Page;
                        await NavigationService.NavigateAsync(new Uri($"Navigation/{SelectItem.Page}", UriKind.Relative));
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message, TAG);
            }
        }
        private void CloseSessionCommandExecuted()
        {
            try
            {
                Profile.Instance.ClearValues();
                AppSettings.Instance.ClearValues();
                NavigationService.NavigateAsync(new Uri("http://termiz.com/Navigation/LogIn", UriKind.Absolute));
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message, TAG);
            }
        }
        public class Menu
        {
            public string Title { get; set; }
            public ImageSource Icon { get; set; }
            public string Page { get; set; }
            public bool ChangeMaster { get; set; }
        }
        #endregion

    }
}
