﻿using Acr.UserDialogs;
using Plugin.Connectivity.Abstractions;
using Prism.Commands;
using Prism.Events;
using Prism.Navigation;
using System;
using System.Diagnostics;
using TermizApp.Abstractions;
using TermizApp.Models.Views;
using TermizApp.Views.EfficiencyBoards;

namespace TermizApp.ViewModels.EfficiencyBoards
{
    public class EfficiencyBoardsMenuPageViewModel : ViewModelBase
    {
        #region Vars
        private static readonly string TAG = nameof(EfficiencyBoardsMenuPageViewModel);
        #endregion

        #region Properties
        public ObservableCollectionExt<EfficiencyBoardsMenuItem> Items { get;  set; }
        private EfficiencyBoardsMenuItem selectItem;
        public EfficiencyBoardsMenuItem SelectItem
        {
            get => selectItem;
            set => SetProperty(ref selectItem, value);
        }
        #endregion

        #region Vars Commands
        public DelegateCommand OnSelectItemCommand { get; set; }
        #endregion

        public EfficiencyBoardsMenuPageViewModel(INavigationService navigationService, 
                                                 IUserDialogs userDialogsService,
                                                 IConnectivity connectivity,
                                                 IEventAggregator eventAggregator) : base(navigationService, userDialogsService, connectivity,eventAggregator)
        {
            TitleToolbar = "TABLEROS DE EFICIENCIA";
            Items = new ObservableCollectionExt<EfficiencyBoardsMenuItem>();
            OnSelectItemCommand = new DelegateCommand(OnSelectItemCommandExecute);
        }

        #region Methodos Commands
        private async void OnSelectItemCommandExecute()
        {
            var navigationParameters = new NavigationParameters
            {
                {"board" , SelectItem}
            };
            await NavigationService.NavigateAsync(nameof(GenericBoardPage),navigationParameters);
        }
        #endregion

        #region Populating Methods
        private void PopulateListViewMenu()
        {
            Items.Add(
                new EfficiencyBoardsMenuItem
                {
                    Board = TypeBoard.ExecutiveBoard,
                    Title = "EJECUTIVO",
                    Image = "ejecutivo.jpg"                   
                }
            );
            Items.Add(
                new EfficiencyBoardsMenuItem
                {
                    Board = TypeBoard.OperativeBoard,
                    Title = "OPERATIVO",
                    Image = "operativo.jpg"
                }
            );
            Items.Add(
                new EfficiencyBoardsMenuItem
                {
                    Board = TypeBoard.TechnicalBoard,
                    Title = "TÉCNICO",
                    Image = "tecnico.jpg"
                }
            );
        }
        #endregion

        #region Life Cicle Page
        public override void OnAppearing()
        {
            base.OnAppearing();
            try
            {
                if(Items != null && Items.Count == 0)
                {
                    PopulateListViewMenu();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message, TAG);
            }
        }
        #endregion
    }
}
