﻿using Acr.UserDialogs;
using Plugin.Connectivity.Abstractions;
using Prism.Commands;
using Prism.Events;
using Prism.Navigation;
using System;
using System.Diagnostics;
using System.Threading.Tasks;
using TermizApp.Abstractions;
using TermizApp.LocalData;
using TermizApp.Models.Catalogs;
using TermizApp.Models.Views;
using TermizApp.Services.Catalogs.GlobalServices;
using TermizApp.Views.EfficiencyBoards;

namespace TermizApp.ViewModels.EfficiencyBoards
{
    public class GenericBoardPageViewModel : ViewModelBase
    {

        #region Vars
        private readonly static string TAG = nameof(GenericBoardPageViewModel);
        private readonly IGlobalServices services;
        #endregion

        #region Properties
        private EfficiencyBoardsMenuItem board;
        public EfficiencyBoardsMenuItem Board
        {
            get => board;
            set => SetProperty(ref board, value);
        }
        public ObservableCollectionExt<RegionModel> Regions { get; set; }
        private RegionModel region;
        public RegionModel Region
        {
            get => region;
            set
            {
                SetProperty(ref region, value);
                if(Region != null)
                {
                    PopulatePickerSite(Region.Id);
                }
            }
        }
        public ObservableCollectionExt<SiteModel> Sites { get; set; }
        private SiteModel site;
        public SiteModel Site
        {
            get => site;
            set
            {
                SetProperty(ref site, value);
                if(Site != null)
                {
                    PopulatePickerEquipment(Site.Id);
                }
            }
        }
        public ObservableCollectionExt<EquipmentModel> Equipments { get; set; }
        private EquipmentModel equipment;
        public EquipmentModel Equipment
        {
            get => equipment;
            set
            {
                SetProperty(ref equipment, value);
                if(Equipment != null)
                {
                    PopulateTags();
                }
            }
        }
        #endregion

        #region Vars Commands
        public DelegateCommand SimulateClickCommand { get; set; }
        #endregion

        #region Constructor
        public GenericBoardPageViewModel(IGlobalServices globalServices,
                                         INavigationService navigationService, 
                                         IUserDialogs userDialogsService, 
                                         IConnectivity connectivity,
                                         IEventAggregator eventAggregator) : base(navigationService, userDialogsService, connectivity, eventAggregator)
        {
            services = globalServices;
            Regions = new ObservableCollectionExt<RegionModel>();
            Sites = new ObservableCollectionExt<SiteModel>();
            Equipments = new ObservableCollectionExt<EquipmentModel>();
            SimulateClickCommand = new DelegateCommand(SimulateClickCommandExecute);
        }
        #endregion

        #region Command Methods
        private async void SimulateClickCommandExecute()
        {
            var navParameters = new NavigationParameters
            {
                { "board", Board }
            };
            await NavigationService.NavigateAsync(nameof(GenericEfficiencyPage),navParameters);
        }
        #endregion

        private async void PopulateTags()
        {

        }

        private async Task PopulatePickerRegion()
        {
            var result = await RunSafeApi(services.GetRegions(Profile.Instance.UserName));
            if (result.Status == TypeReponse.Ok && result.Response != null)
            {
                Regions.Reset(result.Response);
            }
        }

        private async void PopulatePickerSite(long id)
        {
            var result = await RunSafeApi(services.GetSites(id, Profile.Instance.UserName));
            if(result.Status == TypeReponse.Ok && result.Response != null)
            {
                Sites.Reset(result.Response);
            }
        }

        private async void PopulatePickerEquipment(long id)
        {
            var result = await RunSafeApi(services.GetEquipment(id));
            if(result.Status == TypeReponse.Ok && result.Response != null)
            {
                Equipments.Reset(result.Response);
            }
        }

        public async override void OnAppearing()
        {
            try
            {
                await PopulatePickerRegion();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message, TAG);
            }
        }

        #region Navigation Methods
        public override void OnNavigatingTo(INavigationParameters parameters)
        {
            if(parameters.ContainsKey("board"))
            {
                Board = parameters.GetValue<EfficiencyBoardsMenuItem>("board");
                TitleToolbar = $"TABLERO {Board.Title}";
            }
        }
        #endregion
    }
}
