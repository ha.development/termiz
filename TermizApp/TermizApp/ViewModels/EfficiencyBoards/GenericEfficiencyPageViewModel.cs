﻿using Acr.UserDialogs;
using Plugin.Connectivity.Abstractions;
using Prism.Events;
using Prism.Navigation;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using TermizApp.Abstractions;
using TermizApp.LocalData;
using TermizApp.Models.Catalogs;
using TermizApp.Models.Views;
using TermizApp.Services.Catalogs.GlobalServices;
using TermizApp.Views.EfficiencyBoards;
using Xamarin.Forms;

namespace TermizApp.ViewModels.EfficiencyBoards
{
    public class GenericEfficiencyPageViewModel : ViewModelBase
    {

        #region Vars
        private readonly static string TAG = nameof(GenericEfficiencyPageViewModel);
        private readonly IGlobalServices services;
        #endregion

        #region Properties
        public ObservableCollection<CarouselData> items;
        public ObservableCollection<CarouselData> Items
        {
            get { return items; }
            set
            {
                SetProperty(ref items, value);
            }
        }
        private int position;
        public int Position
        {
            get => position;
            set => SetProperty(ref position, value);
        }
        private EfficiencyBoardsMenuItem board;
        public EfficiencyBoardsMenuItem Board
        {
            get => board;
            set => SetProperty(ref board, value);
        }
        public ObservableCollectionExt<RegionModel> Regions { get; set; }
        #endregion

        public GenericEfficiencyPageViewModel(IGlobalServices globalServices,
                                              INavigationService navigationService,
                                              IUserDialogs userDialogsService, 
                                              IConnectivity connectivity,
                                              IEventAggregator eventAggregator) : base(navigationService, userDialogsService, connectivity, eventAggregator)
        {
            services = globalServices;
            Regions = new ObservableCollectionExt<RegionModel>();
            Items = new ObservableCollection<CarouselData>();
        }

        private async Task PopulatePickerRegion()
        {
            var result = await RunSafeApi(services.GetRegions("alexp"));
            if(result.Status == TypeReponse.Ok && result.Response != null)
            {
                Regions.Reset(result.Response);
            }
        }

        public async override void OnAppearing()
        {
            try
            {
                await PopulatePickerRegion();
                PopulatingCarousel();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message, TAG);
            }
        }
        private void PopulatingCarousel()
        {
            Items = new ObservableCollection<CarouselData>
            {
                new CarouselData{Content = new GenericEfficiencyContentView(){ BindingContext = this } },
                new CarouselData{Content = new GenericEfficiencyContentView() { BindingContext = this }}
            };
        }

        #region Navigation Methods
        public override void OnNavigatingTo(INavigationParameters parameters)
        {
            if (parameters.ContainsKey("board"))
            {
                Board = parameters.GetValue<EfficiencyBoardsMenuItem>("board");
                TitleToolbar = $"EFICIENCIA {Board.Title}";
            }
        }
        #endregion

    }



    public class CarouselData
    {
        public ContentView Content { get; set; }
    }
}
