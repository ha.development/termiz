﻿using Acr.UserDialogs;
using Plugin.Connectivity.Abstractions;
using Prism.Commands;
using Prism.Events;
using Prism.Navigation;
using System.Drawing;
using TermizApp.Abstractions;
using TermizApp.Models.Catalogs;
using TermizApp.Services.Catalogs.GlobalServices;

namespace TermizApp.ViewModels.Popups
{
    public class DetailEquimentPopupViewModel : ViewModelBase
    {
        #region Vars Commands
        public DelegateCommand OnCloseModalCommand { get; set; }
        #endregion

        #region Properties
        private EquipmentStatesModel equipmentStatesModel;
        public EquipmentStatesModel EquipmentStatesModel
        {
            get => equipmentStatesModel;
            set => SetProperty(ref equipmentStatesModel, value);
        }
        public ObservableCollectionExt<EventEquipment> Items { get; set; }
        #endregion

        #region Constructor
        public DetailEquimentPopupViewModel(INavigationService navigationService,
                                            IUserDialogs userDialogsService,
                                            IConnectivity connectivity,
                                            IEventAggregator eventAggregator) : base(navigationService, userDialogsService, connectivity, eventAggregator)
        {
            OnCloseModalCommand = new DelegateCommand(() => NavigationService.GoBackAsync());
            Items = new ObservableCollectionExt<EventEquipment>();            
        }
        #endregion

        #region Populating Methods
        private void PopulateListViewEvents()
        {
            if(EquipmentStatesModel.Eventos != null && EquipmentStatesModel.Eventos.Count > 0)
            {
                foreach (var item in EquipmentStatesModel.Eventos)
                {
                    //switch (item.Nivel)
                    //{
                    //    case long n when (n < 3):
                    //        item.Image = "esferaverde.png";
                    //        break;
                    //    case long n when (n > 2 && n < 6):
                    //        item.Image = "esferaamarilla.png";
                    //        break;
                    //    case long n when (n > 5):
                    //        item.Image = "esferaroja.png";
                    //        break;
                    //}
                    
                    if (item.FechaFin == null)
                    {
                        item.DescripcionFecha = string.Format("{0} - Actual", item.FechaInicio.Value.ToLongDateString());
                    }
                    else
                    {
                        item.DescripcionFecha = item.DescripcionFecha = string.Format("{0} - {1}", item.FechaInicio.Value.ToString(), item.FechaFin.Value.ToString());
                    }
                }
                Items.Reset(EquipmentStatesModel.Eventos);
            }
        }
        #endregion

        #region Navigation Methods
        public override void OnNavigatingTo(INavigationParameters parameters)
        {
            base.OnNavigatingTo(parameters);
            if(parameters.ContainsKey("item"))
            {
                EquipmentStatesModel = parameters.GetValue<EquipmentStatesModel>("item");
                PopulateListViewEvents();
            }
        }
        #endregion
    }
}
