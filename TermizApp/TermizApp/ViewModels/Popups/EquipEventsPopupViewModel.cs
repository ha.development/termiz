﻿using Acr.UserDialogs;
using Plugin.Connectivity.Abstractions;
using Prism.Commands;
using Prism.Events;
using Prism.Navigation;
using TermizApp.Abstractions;
using TermizApp.Models.Catalogs;

namespace TermizApp.ViewModels.Popups
{
    public class EquipEventsPopupViewModel : ViewModelBase
    {

        #region Properties
        private EquipmentStatesModel equipmentStatesModel;
        public EquipmentStatesModel EquipmentStatesModel
        {
            get => equipmentStatesModel;
            set => SetProperty(ref equipmentStatesModel, value);
        }
        #endregion

        #region Vars Commands
        public DelegateCommand OnCloseModalCommand { get; set; }
        #endregion

        public EquipEventsPopupViewModel(INavigationService navigationService, IUserDialogs userDialogsService, IConnectivity connectivity, IEventAggregator eventAggregator) : base(navigationService, userDialogsService, connectivity, eventAggregator)
        {
            OnCloseModalCommand = new DelegateCommand(() => NavigationService.GoBackAsync());
        }

        #region Navigation Methods
        public override void OnNavigatingTo(INavigationParameters parameters)
        {
            base.OnNavigatingTo(parameters);
            if (parameters.ContainsKey("item"))
            {
                EquipmentStatesModel = parameters.GetValue<EquipmentStatesModel>("item");
            }
        }
        #endregion
    }
}
