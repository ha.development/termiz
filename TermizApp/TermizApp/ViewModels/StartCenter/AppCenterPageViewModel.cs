﻿using Acr.UserDialogs;
using Plugin.Connectivity.Abstractions;
using Prism.Events;
using Prism.Navigation;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using TermizApp.Abstractions;
using TermizApp.Events;
using TermizApp.LocalData;
using TermizApp.Models.Catalogs;
using TermizApp.Services.Catalogs.GlobalServices;
using Xamarin.Forms.GoogleMaps;

namespace TermizApp.ViewModels.StartCenter
{
    public class StartCenterPageViewModel : ViewModelBase
    {

        #region Vars
        private readonly IGlobalServices services;
        private List<long?> States;
        #endregion

        #region Properties
        private long countState;
        public long CountState
        {
            get => countState;
            set => SetProperty(ref countState, value);
        }
        private long countEquipment;
        public long CountEqument
        {
            get => countEquipment;
            set => SetProperty(ref countEquipment, value);
        }
        private Pin selectItem;
        public Pin SelectItem
        {
            get => selectItem;
            set
            {
                SetProperty(ref selectItem, value);
                if(SelectItem != null)
                {
                    SelectItemMethod(SelectItem);
                }
            }
        }
        private ObservableCollection<EquipmentStatesModel> items;
        public ObservableCollection<EquipmentStatesModel> Items
        {
            get => items;
            set => SetProperty(ref items, value);
        }
        #endregion

        public StartCenterPageViewModel(IGlobalServices globalServices,
                                        INavigationService navigationService, 
                                        IUserDialogs userDialogsService, 
                                        IConnectivity connectivity, 
                                        IEventAggregator eventAggregator) : base(navigationService, userDialogsService, connectivity, eventAggregator)
        {
            services = globalServices;
            Items = new ObservableCollection<EquipmentStatesModel>();
            TitleToolbar = "START CENTER";           

            Events.GetEvent<ChangeOfClientEvent>().Subscribe(UpdateInformationClient);
        }


        private async void SelectItemMethod(Pin item)
        {
            var equipment = Items.FirstOrDefault(x => x.Latitud.Equals(item.Position.Latitude) && x.Longitud.Equals(item.Position.Longitude));
            if(equipment != null)
            {
                await NavigationService.NavigateAsync("DetailEquiment", new NavigationParameters { { "item", equipment } });
            }
        }

        private async void UpdateInformationClient(long obj)
        {
            if (IsBusy)
                return;            

            IsBusy = true;
            var result = await RunSafeApi(services.GetEquipmentStates(obj));
            if(result.Status == TypeReponse.Ok)
            {
                if(result.Response != null)
                {
                    States = new List<long?>();
                    Items.Clear();                    
                    foreach (var item in result.Response)
                    {
                        item.Position = new Position(item.Latitud, item.Longitud);                       
                        item.Icon = GetIconPin(item.Color);
                        var state = States.FirstOrDefault(x => x == item.IdState);
                        if(state is null)
                        {
                            States.Add(item.IdState);
                        }
                        CountEqument++;
                        Items.Add(item);                        
                    }
                    
                    CountState = States.Count();
                }
            }
            IsBusy = false;
        }

        private BitmapDescriptor GetIconPin(long item)
        {
            switch(item)
            {
                case 0:
                    return BitmapDescriptorFactory.FromBundle("gray.png");
                case 1:
                    return BitmapDescriptorFactory.FromBundle("green.png");
                case 2:
                    return BitmapDescriptorFactory.FromBundle("yellow.png");
                case 3:
                    return BitmapDescriptorFactory.FromBundle("red.png");
            }
            return BitmapDescriptorFactory.FromBundle("gray.png");
        }
    }
}
