﻿using Xamarin.Forms;
using TermizApp.Models.Bases;

namespace TermizApp.Controls.Bases
{
    public class LabelFontBase : Label
    {
        /// <summary>
        /// Property definition for the <see cref="Fonts"/> Property
        /// </summary>
        public static readonly BindableProperty FontLabelProperty =
        BindableProperty.Create(propertyName: nameof(FontLabel),
              returnType: typeof(Fonts),
              declaringType: typeof(LabelFontBase),
              defaultValue: Fonts.None);

        public static readonly BindableProperty FontNamedSizeProperty =
        BindableProperty.Create(
            "FontNamedSize", typeof(NamedSize), typeof(LabelFontBase),
            defaultValue: default(NamedSize), propertyChanged: OnFontNamedSizeChanged);

        public NamedSize FontNamedSize
        {
            get { return (NamedSize)GetValue(FontNamedSizeProperty); }
            set { SetValue(FontNamedSizeProperty, value); }
        }

        private static void OnFontNamedSizeChanged(BindableObject bindable, object oldValue, object newValue)
        {
            ((LabelFontBase)bindable).OnFontNamedSizeChangedImpl((NamedSize)oldValue, (NamedSize)newValue);
        }

        protected virtual void OnFontNamedSizeChangedImpl(NamedSize oldValue, NamedSize newValue)
        {
            FontSize = Device.GetNamedSize(FontNamedSize, typeof(Label));
        }

        /// <summary>
        /// Gets or sets the font.
        /// </summary>
        /// <value>
        /// The label.
        /// </value>
        public Fonts FontLabel
        {
            get { return (Fonts)GetValue(FontLabelProperty); }
            set { SetValue(FontLabelProperty, value); }
        }        
    }
}
