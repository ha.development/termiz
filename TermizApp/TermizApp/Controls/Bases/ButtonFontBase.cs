﻿using TermizApp.Models.Bases;
using Xamarin.Forms;

namespace TermizApp.Controls.Bases
{
    public class ButtonFontBase : Button
    {
        /// <summary>
        /// Property definition for the <see cref="Fonts"/> Property
        /// </summary>
        public static readonly BindableProperty FontLabelProperty =
        BindableProperty.Create(propertyName: nameof(FontLabel),
              returnType: typeof(Fonts),
              declaringType: typeof(ButtonFontBase),
              defaultValue: Fonts.None);

        /// <summary>
        /// Gets or sets the font.
        /// </summary>
        /// <value>
        /// The label.
        /// </value>
        public Fonts FontLabel
        {
            get { return (Fonts)GetValue(FontLabelProperty); }
            set { SetValue(FontLabelProperty, value); }
        }
    }
}
