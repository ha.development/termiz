﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TermizApp.Controls.ViewCells
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class VariableListViewCell : ViewCell
    {
        public VariableListViewCell()
        {
            InitializeComponent();
        }
    }
}