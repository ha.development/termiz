﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TermizApp.Controls.ViewCells
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EfficiencyBoardsMenuViewCell : ViewCell
	{
		public EfficiencyBoardsMenuViewCell ()
		{
			InitializeComponent ();
		}
	}
}