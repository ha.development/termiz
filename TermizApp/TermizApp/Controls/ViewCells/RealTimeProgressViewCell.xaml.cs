﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TermizApp.Controls.ViewCells
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RealTimeProgressViewCell : ViewCell
	{
		public RealTimeProgressViewCell ()
		{
			InitializeComponent ();
		}
	}
}