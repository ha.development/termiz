﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TermizApp.Controls.ViewCells
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PinCustom : ContentView
	{
        private string _display;
        public PinCustom (string display)
		{
			InitializeComponent ();
            _display = display;
            BindingContext = this;
        }
        public string Display
        {
            get { return _display; }
        }
    }
}