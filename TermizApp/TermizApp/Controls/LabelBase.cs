﻿using System.Runtime.CompilerServices;
using TermizApp.Models.Bases;
using Xamarin.Forms;

namespace TermizApp.Controls
{
    public class LabelBase : Label
    {
        public static readonly BindableProperty TypeFontProperty =
            BindableProperty.Create(nameof(TypeFont), typeof(Fonts),
                                    typeof(LabelBase), Fonts.None);

        public Fonts TypeFont
        {
            get => (Fonts)GetValue(TypeFontProperty);
            set => SetValue(TypeFontProperty, value);
        }

        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);
            if (propertyName == TypeFontProperty.PropertyName)
            {
                switch (TypeFont)
                {
                    case Fonts.Ubuntu:
                        FontFamily = Device.RuntimePlatform.Equals(Device.iOS) ? "Ubuntu-R" : "Ubuntu-R.ttf#Ubuntu-R";
                        break;
                    case Fonts.QuicksandLight:
                        FontFamily = Device.RuntimePlatform.Equals(Device.iOS) ? "Quicksand-Light" : "Quicksand-Light.ttf#Quicksand-Light";
                        break;
                }
            }
        }

    }
}
