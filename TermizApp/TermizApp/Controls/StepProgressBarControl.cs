﻿using System;
using System.Linq;
using Xamarin.Forms;

namespace TermizApp.Controls
{
    public class StepProgressBarControl : StackLayout
    {
        Button _lastStepSelected;
        public static readonly BindableProperty StepsProperty = BindableProperty.Create(nameof(Steps), typeof(int), typeof(StepProgressBarControl), 0);
        public static readonly BindableProperty StepSelectedProperty = BindableProperty.Create(nameof(StepSelected), typeof(int), typeof(StepProgressBarControl), 0, defaultBindingMode: BindingMode.TwoWay);
        public static readonly BindableProperty StepColorProperty = BindableProperty.Create(nameof(StepColor), typeof(Xamarin.Forms.Color), typeof(StepProgressBarControl), Color.Black, defaultBindingMode: BindingMode.TwoWay);

        public Color StepColor
        {
            get { return (Color)GetValue(StepColorProperty); }
            set { SetValue(StepColorProperty, value); }
        }

        public int Steps
        {
            get { return (int)GetValue(StepsProperty); }
            set { SetValue(StepsProperty, value); }
        }

        public int StepSelected
        {
            get { return (int)GetValue(StepSelectedProperty); }
            set { SetValue(StepSelectedProperty, value); }
        }


        public StepProgressBarControl()
        {
            Orientation = StackOrientation.Horizontal;
            HorizontalOptions = LayoutOptions.FillAndExpand;
            Padding = new Thickness(10, 0);
            Spacing = 0;
            AddStyles();

        }

        protected override void OnPropertyChanged(string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);

            if (propertyName == StepsProperty.PropertyName)
            {
                ActualizaPaso(Steps);
            }
        }

        void Handle_Clicked(object sender, System.EventArgs e)
        {
            SelectElement(sender as Button);
        }


        void ActualizaPaso(int paso)
        {
            var imagen = new Image();
            var separatorLine = new BoxView();
            this.Children.Clear();
            switch (paso)
            {
                case 1:

                    if(Device.RuntimePlatform == Device.iOS)
                    {
                        imagen = new Image()
                        {
                            Source = "ic_option_check",
                            Scale = 0.7
                        };    
                    }
                    else if(Device.RuntimePlatform == Device.Android)
                    {
                        imagen = new Image()
                        {
                            Source = "ic_option_check",
                            Scale = 1.2,
                            HeightRequest = 20,
                            WidthRequest = 20
                        };
                    }
                    this.Children.Add(imagen);

                    if (Device.RuntimePlatform == Device.iOS)
                    {
                        separatorLine = new BoxView()
                        {
                            BackgroundColor = Color.FromHex("#e0e0e0"),
                            HeightRequest = 2,
                            WidthRequest = 10,
                            VerticalOptions = LayoutOptions.Center,
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            Margin = -5
                        };
                    }
                    else if (Device.RuntimePlatform == Device.Android)
                    {
                        separatorLine = new BoxView()
                        {
                            BackgroundColor = Color.FromHex("#e0e0e0"),
                            HeightRequest = 2,
                            WidthRequest = 10,
                            VerticalOptions = LayoutOptions.Center,
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            Margin = -1
                        };
                    }
                    this.Children.Add(separatorLine);

                    if (Device.RuntimePlatform == Device.iOS)
                    {
                        imagen = new Image()
                        {
                            Source = "ic_notactive_check",
                            Scale = 0.7
                        };
                    }
                    else if (Device.RuntimePlatform == Device.Android)
                    {
                        imagen = new Image()
                        {
                            Source = "ic_notactive_check",
                            Scale = 1.2,
                            HeightRequest = 20,
                            WidthRequest = 20
                        };
                    }
                    this.Children.Add(imagen);

                    if (Device.RuntimePlatform == Device.iOS)
                    {
                        separatorLine = new BoxView()
                       {
                           BackgroundColor = Color.FromHex("#e0e0e0"),
                           HeightRequest = 2,
                           WidthRequest = 10,
                           VerticalOptions = LayoutOptions.Center,
                           HorizontalOptions = LayoutOptions.FillAndExpand,
                           Margin = -8
                       };
                    }
                    else if (Device.RuntimePlatform == Device.Android)
                    {
                        separatorLine = new BoxView()
                        {
                            BackgroundColor = Color.FromHex("#e0e0e0"),
                            HeightRequest = 2,
                            WidthRequest = 10,
                            VerticalOptions = LayoutOptions.Center,
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            Margin = -5
                        };
                    }
                    this.Children.Add(separatorLine);

                    if (Device.RuntimePlatform == Device.iOS)
                    {
                        imagen = new Image()
                        {
                            Source = "ic_notactive_check",
                            Scale = 0.7
                        };
                    }
                    else if (Device.RuntimePlatform == Device.Android)
                    {
                        imagen = new Image()
                        {
                            Source = "ic_notactive_check",
                            Scale = 1.2,
                            HeightRequest = 20,
                            WidthRequest = 20
                        };
                    }
                    this.Children.Add(imagen);
                    break;
                case 2:
                    if (Device.RuntimePlatform == Device.iOS)
                    {
                        imagen = new Image()
                        {
                            Source = "ic_option_acept",
                            Scale = 0.7
                        };
                    }
                    else if (Device.RuntimePlatform == Device.Android)
                    {
                        imagen = new Image()
                        {
                            Source = "ic_option_acept",
                            Scale = 1.2,
                            HeightRequest = 20,
                            WidthRequest = 20
                        };
                    }
                    this.Children.Add(imagen);

                    if (Device.RuntimePlatform == Device.iOS)
                    {
                        separatorLine = new BoxView()
                        {
                            BackgroundColor = Color.FromHex("#e0e0e0"),
                            HeightRequest = 2,
                            WidthRequest = 10,
                            VerticalOptions = LayoutOptions.Center,
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            Margin = -9
                        };
                    }
                    else if (Device.RuntimePlatform == Device.Android)
                    {
                        separatorLine = new BoxView()
                        {
                            BackgroundColor = Color.FromHex("#e0e0e0"),
                            HeightRequest = 2,
                            WidthRequest = 10,
                            VerticalOptions = LayoutOptions.Center,
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            Margin = -1
                        };
                    }
                    this.Children.Add(separatorLine);

                    if (Device.RuntimePlatform == Device.iOS)
                    {
                        imagen = new Image()
                        {
                            Source = "ic_notactive_check",
                            Scale = 0.7
                        };
                    }
                    else if (Device.RuntimePlatform == Device.Android)
                    {
                        imagen = new Image()
                        {
                            Source = "ic_notactive_check",
                            Scale = 1.2,
                            HeightRequest = 20,
                            WidthRequest = 20
                        };
                    }
                    this.Children.Add(imagen);

                    if (Device.RuntimePlatform == Device.iOS)
                    {
                        separatorLine = new BoxView()
                        {
                            BackgroundColor = Color.FromHex("#e0e0e0"),
                            HeightRequest = 2,
                            WidthRequest = 10,
                            VerticalOptions = LayoutOptions.Center,
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            Margin = -8
                        };
                    }
                    else if (Device.RuntimePlatform == Device.Android)
                    {
                        separatorLine = new BoxView()
                        {
                            BackgroundColor = Color.FromHex("#e0e0e0"),
                            HeightRequest = 2,
                            WidthRequest = 10,
                            VerticalOptions = LayoutOptions.Center,
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            Margin = -5
                        };
                    }
                    this.Children.Add(separatorLine);

                    if (Device.RuntimePlatform == Device.iOS)
                    {
                        imagen = new Image()
                        {
                            Source = "ic_notactive_check",
                            Scale = 0.7
                        };
                    }
                    else if (Device.RuntimePlatform == Device.Android)
                    {
                        imagen = new Image()
                        {
                            Source = "ic_notactive_check",
                            Scale = 1.2,
                            HeightRequest = 20,
                            WidthRequest = 20
                        };
                    }
                    this.Children.Add(imagen);
                    break;
                case 3:
                    if (Device.RuntimePlatform == Device.iOS)
                    {
                        imagen = new Image()
                        {
                            Source = "ic_option_acept",
                            Scale = 0.7
                        };
                    }
                    else if (Device.RuntimePlatform == Device.Android)
                    {
                        imagen = new Image()
                        {
                            Source = "ic_option_acept",
                            Scale = 1.2,
                            HeightRequest = 20,
                            WidthRequest = 20
                        };
                    }
                    this.Children.Add(imagen);

                    if (Device.RuntimePlatform == Device.iOS)
                    {
                        separatorLine = new BoxView()
                        {
                            BackgroundColor = Color.FromHex("#00b0ad"),
                            HeightRequest = 2,
                            WidthRequest = 10,
                            VerticalOptions = LayoutOptions.Center,
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            Margin = -9
                        };
                    }
                    else if (Device.RuntimePlatform == Device.Android)
                    {
                        separatorLine = new BoxView()
                        {
                            BackgroundColor = Color.FromHex("#00b0ad"),
                            HeightRequest = 2,
                            WidthRequest = 10,
                            VerticalOptions = LayoutOptions.Center,
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            Margin = -1
                        };
                    }
                    this.Children.Add(separatorLine);

                    if (Device.RuntimePlatform == Device.iOS)
                    {
                        imagen = new Image()
                        {
                            Source = "ic_option_check",
                            Scale = 0.7
                        };
                    }
                    else if (Device.RuntimePlatform == Device.Android)
                    {
                        imagen = new Image()
                        {
                            Source = "ic_option_check",
                            Scale = 1.2,
                            HeightRequest = 20,
                            WidthRequest = 20
                        };
                    }
                    this.Children.Add(imagen);

                    if (Device.RuntimePlatform == Device.iOS)
                    {
                        separatorLine = new BoxView()
                        {
                            BackgroundColor = Color.FromHex("#e0e0e0"),
                            HeightRequest = 2,
                            WidthRequest = 10,
                            VerticalOptions = LayoutOptions.Center,
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            Margin = -4
                        };
                    }
                    else if (Device.RuntimePlatform == Device.Android)
                    {
                        separatorLine = new BoxView()
                        {
                            BackgroundColor = Color.FromHex("#e0e0e0"),
                            HeightRequest = 2,
                            WidthRequest = 10,
                            VerticalOptions = LayoutOptions.Center,
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            Margin = -1
                        };
                    }
                    this.Children.Add(separatorLine);

                    if (Device.RuntimePlatform == Device.iOS)
                    {
                        imagen = new Image()
                        {
                            Source = "ic_notactive_check",
                            Scale = 0.7
                        };
                    }
                    else if (Device.RuntimePlatform == Device.Android)
                    {
                        imagen = new Image()
                        {
                            Source = "ic_notactive_check",
                            Scale = 1.2,
                            HeightRequest = 20,
                            WidthRequest = 20
                        };
                    }
                    this.Children.Add(imagen);
                    break;
                case 4:
                    if (Device.RuntimePlatform == Device.iOS)
                    {
                        imagen = new Image()
                        {
                            Source = "ic_option_acept",
                            Scale = 0.7
                        };
                    }
                    else if (Device.RuntimePlatform == Device.Android)
                    {
                        imagen = new Image()
                        {
                            Source = "ic_option_acept",
                            Scale = 1.2,
                            HeightRequest = 20,
                            WidthRequest = 20
                        };
                    }
                    this.Children.Add(imagen);

                    if (Device.RuntimePlatform == Device.iOS)
                    {
                        separatorLine = new BoxView()
                        {
                            BackgroundColor = Color.FromHex("#00b0ad"),
                            HeightRequest = 2,
                            WidthRequest = 10,
                            VerticalOptions = LayoutOptions.Center,
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            Margin = -9
                        };
                    }
                    else if (Device.RuntimePlatform == Device.Android)
                    {
                        separatorLine = new BoxView()
                        {
                            BackgroundColor = Color.FromHex("#00b0ad"),
                            HeightRequest = 2,
                            WidthRequest = 10,
                            VerticalOptions = LayoutOptions.Center,
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            Margin = -1
                        };
                    }
                    this.Children.Add(separatorLine);

                    if (Device.RuntimePlatform == Device.iOS)
                    {
                        imagen = new Image()
                        {
                            Source = "ic_option_acept",
                            Scale = 0.7
                        };
                    }
                    else if (Device.RuntimePlatform == Device.Android)
                    {
                        imagen = new Image()
                        {
                            Source = "ic_option_acept",
                            Scale = 1.2,
                            HeightRequest = 20,
                            WidthRequest = 20
                        };
                    }
                    this.Children.Add(imagen);

                    if (Device.RuntimePlatform == Device.iOS)
                    {
                        separatorLine = new BoxView()
                        {
                            BackgroundColor = Color.FromHex("#00b0ad"),
                            HeightRequest = 2,
                            WidthRequest = 10,
                            VerticalOptions = LayoutOptions.Center,
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            Margin = -9
                        };
                    }
                    else if (Device.RuntimePlatform == Device.Android)
                    {
                        separatorLine = new BoxView()
                        {
                            BackgroundColor = Color.FromHex("#00b0ad"),
                            HeightRequest = 2,
                            WidthRequest = 10,
                            VerticalOptions = LayoutOptions.Center,
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            Margin = -6
                        };
                    }
                    this.Children.Add(separatorLine);

                    if (Device.RuntimePlatform == Device.iOS)
                    {
                        imagen = new Image()
                        {
                            Source = "ic_option_check",
                            Scale = 0.7
                        };
                    }
                    else if (Device.RuntimePlatform == Device.Android)
                    {
                        imagen = new Image()
                        {
                            Source = "ic_option_check",
                            Scale = 1.2,
                            HeightRequest = 20,
                            WidthRequest = 20
                        };
                    }
                    this.Children.Add(imagen);
                    break;
                case 5:
                    if (Device.RuntimePlatform == Device.iOS)
                    {
                        imagen = new Image()
                        {
                            Source = "ic_option_acept",
                            Scale = 0.7
                        };
                    }
                    else if (Device.RuntimePlatform == Device.Android)
                    {
                        imagen = new Image()
                        {
                            Source = "ic_option_acept",
                            Scale = 1.2,
                            HeightRequest = 20,
                            WidthRequest = 20
                        };
                    }
                    this.Children.Add(imagen);

                    if (Device.RuntimePlatform == Device.iOS)
                    {
                        separatorLine = new BoxView()
                        {
                            BackgroundColor = Color.FromHex("#00b0ad"),
                            HeightRequest = 2,
                            WidthRequest = 10,
                            VerticalOptions = LayoutOptions.Center,
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            Margin = -9
                        };
                    }
                    else if (Device.RuntimePlatform == Device.Android)
                    {
                        separatorLine = new BoxView()
                        {
                            BackgroundColor = Color.FromHex("#00b0ad"),
                            HeightRequest = 2,
                            WidthRequest = 10,
                            VerticalOptions = LayoutOptions.Center,
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            Margin = -1
                        };
                    }
                    this.Children.Add(separatorLine);

                    if (Device.RuntimePlatform == Device.iOS)
                    {
                        imagen = new Image()
                        {
                            Source = "ic_option_acept",
                            Scale = 0.7
                        };
                    }
                    else if (Device.RuntimePlatform == Device.Android)
                    {
                        imagen = new Image()
                        {
                            Source = "ic_option_acept",
                            Scale = 1.2,
                            HeightRequest = 20,
                            WidthRequest = 20
                        };
                    }
                    this.Children.Add(imagen);

                    if (Device.RuntimePlatform == Device.iOS)
                    {
                        separatorLine = new BoxView()
                        {
                            BackgroundColor = Color.FromHex("#00b0ad"),
                            HeightRequest = 2,
                            WidthRequest = 10,
                            VerticalOptions = LayoutOptions.Center,
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            Margin = -9
                        };
                    }
                    else if (Device.RuntimePlatform == Device.Android)
                    {
                        separatorLine = new BoxView()
                        {
                            BackgroundColor = Color.FromHex("#00b0ad"),
                            HeightRequest = 2,
                            WidthRequest = 10,
                            VerticalOptions = LayoutOptions.Center,
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            Margin = -6
                        };
                    }
                    this.Children.Add(separatorLine);

                    if (Device.RuntimePlatform == Device.iOS)
                    {
                        imagen = new Image()
                        {
                            Source = "ic_option_acept",
                            Scale = 0.7
                        };
                    }
                    else if (Device.RuntimePlatform == Device.Android)
                    {
                        imagen = new Image()
                        {
                            Source = "ic_option_acept",
                            Scale = 1.2,
                            HeightRequest = 20,
                            WidthRequest = 20
                        };
                    }
                    this.Children.Add(imagen);
                    break;
                default:
                    if (Device.RuntimePlatform == Device.iOS)
                    {
                        imagen = new Image()
                        {
                            Source = "ic_option_acept",
                            Scale = 0.7
                        };
                    }
                    else if (Device.RuntimePlatform == Device.Android)
                    {
                        imagen = new Image()
                        {
                            Source = "ic_option_acept",
                            Scale = 1.2,
                            HeightRequest = 20,
                            WidthRequest = 20
                        };
                    }
                    this.Children.Add(imagen);

                    if (Device.RuntimePlatform == Device.iOS)
                    {
                        separatorLine = new BoxView()
                        {
                            BackgroundColor = Color.FromHex("#e0e0e0"),
                            HeightRequest = 2,
                            WidthRequest = 10,
                            VerticalOptions = LayoutOptions.Center,
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            Margin = -5
                        };
                    }
                    else if (Device.RuntimePlatform == Device.Android)
                    {
                        separatorLine = new BoxView()
                        {
                            BackgroundColor = Color.FromHex("#e0e0e0"),
                            HeightRequest = 2,
                            WidthRequest = 10,
                            VerticalOptions = LayoutOptions.Center,
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            Margin = -1
                        };
                    }
                    this.Children.Add(separatorLine);

                    if (Device.RuntimePlatform == Device.iOS)
                    {
                        imagen = new Image()
                        {
                            Source = "ic_notactive_check",
                            Scale = 0.7
                        };
                    }
                    else if (Device.RuntimePlatform == Device.Android)
                    {
                        imagen = new Image()
                        {
                            Source = "ic_notactive_check",
                            Scale = 1.2,
                            HeightRequest = 20,
                            WidthRequest = 20
                        };
                    }
                    this.Children.Add(imagen);

                    if (Device.RuntimePlatform == Device.iOS)
                    {
                        separatorLine = new BoxView()
                        {
                            BackgroundColor = Color.FromHex("#e0e0e0"),
                            HeightRequest = 2,
                            WidthRequest = 10,
                            VerticalOptions = LayoutOptions.Center,
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            Margin = -8
                        };
                    }
                    else if (Device.RuntimePlatform == Device.Android)
                    {
                        separatorLine = new BoxView()
                        {
                            BackgroundColor = Color.FromHex("#e0e0e0"),
                            HeightRequest = 2,
                            WidthRequest = 10,
                            VerticalOptions = LayoutOptions.Center,
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            Margin = -5
                        };
                    }
                    this.Children.Add(separatorLine);

                    if (Device.RuntimePlatform == Device.iOS)
                    {
                        imagen = new Image()
                        {
                            Source = "ic_notactive_check",
                            Scale = 0.7
                        };
                    }
                    else if (Device.RuntimePlatform == Device.Android)
                    {
                        imagen = new Image()
                        {
                            Source = "ic_notactive_check",
                            Scale = 1.2,
                            HeightRequest = 20,
                            WidthRequest = 20
                        };
                    }
                    this.Children.Add(imagen);
                    break;
            }
        }

        void SelectElement(Button elementSelected)
        {

            if (_lastStepSelected != null) _lastStepSelected.Style = Resources["unSelectedStyle"] as Style;

            elementSelected.Style = Resources["selectedStyle"] as Style;

            StepSelected = Convert.ToInt32(elementSelected.Text);
            _lastStepSelected = elementSelected;

        }

        void AddStyles()
        {
            var unselectedStyle = new Style(typeof(Button))
            {
                Setters = {
                    new Setter { Property = BackgroundColorProperty,   Value = Color.Transparent },
                    new Setter { Property = Button.BorderColorProperty,   Value = StepColor }, //StepColor
                    new Setter { Property = Button.TextColorProperty,   Value = Color.Transparent },
                    new Setter { Property = Button.BorderWidthProperty,   Value = 0.5 },
                    new Setter { Property = Button.BorderRadiusProperty,   Value = 10 },
                    new Setter { Property = Button.BorderWidthProperty,   Value = 2 },
                    new Setter { Property = HeightRequestProperty,   Value = 20 },
                    //new Setter { Property = Button.ImageProperty,   Value = "ic_active_check.png" },
                    new Setter { Property = WidthRequestProperty,   Value = 20 }
            }
            };

            var selectedStyle = new Style(typeof(Button))
            {
                Setters = {
                    new Setter { Property = BackgroundColorProperty, Value = StepColor },
                    new Setter { Property = Button.TextColorProperty, Value = Color.Transparent },
                    new Setter { Property = Button.BorderColorProperty, Value = StepColor },
                    new Setter { Property = Button.BorderWidthProperty,   Value = 0.5 },
                    new Setter { Property = Button.BorderRadiusProperty,   Value = 10 },
                    new Setter { Property = HeightRequestProperty,   Value = 20 },
                    new Setter { Property = WidthRequestProperty,   Value = 20 },
                    new Setter { Property = Button.FontAttributesProperty,   Value = FontAttributes.Bold }
            }
            };

            Resources = new ResourceDictionary();
            Resources.Add("unSelectedStyle", unselectedStyle);
            Resources.Add("selectedStyle", selectedStyle);
        }
    }
}