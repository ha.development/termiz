﻿public static class Tags
{
    public readonly static string TEMPERATURA_AIRE = "TAG00000024";
    public readonly static string OXIGENO_GASES = "TAG00000019";
    public readonly static string EFICIENCIA_COMBUSTIBLE_VAPOR = "TAG00000029";
    public readonly static string TEMPERATURA_AGUA_ENTRADA = "TAG00000026";
    public readonly static string EFICIENCIA_POR_AGUA_DE_ALIMENTACION = "TAG00000030";
    public readonly static string PRESION_VAPOR = "TAG00000022";
    public readonly static string CALDERA = "TAG00000018";
}

