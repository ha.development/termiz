﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Refit;
using TermizApp.Models.Catalogs;

namespace TermizApp.Services.Session
{
    public interface ISessionService
    {
        [Post("/LDAPAuthentication")]
        [Headers("Authorization: Bearer")]
        Task<bool> AuthUser(string username, string password);

        [Get("/Spartan_User/ListaSelAll?startRowIndex=1&maximumRows=1&Where=Username='{username}' COLLATE SQL_Latin1_General_CP1_CS_AS And Password='{password}' COLLATE SQL_Latin1_General_CP1_CS_AS")]
        [Headers("Authorization: Bearer")]
        Task<SpartanUserList> AuthUserSpartane(string username, string password);

        [Get("/Vertica_Catalogos/GetUsuario")]
        [Headers("Authorization: Bearer")]
        Task<UserModel> GetUser(string username);

        [Get("/Vertica_Catalogos/GetClientes")]
        [Headers("Authorization: Bearer")]
        Task<List<UserModel>> GetClients();

        [Get("/Spartan_File/Get?Id={Id}")]
        [Headers("Authorization: Bearer")]
        Task<SpartaneFileModel> GetFile(long? id);
    }
}
