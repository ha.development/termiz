﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Refit;
using TermizApp.Helpers;
using TermizApp.Models.Catalogs;
using TermizApp.Settings;

namespace TermizApp.Services.Session
{
    public class SessionService : ISessionService
    {
        private readonly ISessionService sessionService;

        public SessionService()
        {
            sessionService = RestService.For<ISessionService>(new HttpClient(new HttpLoggingHandler(TokenManager.GetToken)) { BaseAddress = new Uri(AppConfiguration.Values.BaseUrl) }, new RefitSettings
            {
                JsonSerializerSettings = new JsonSerializerSettings
                {
                    ContractResolver = new CustomResolver()
                }
            });
        }

        public Task<bool> AuthUser(string username, string password)
        {
            return sessionService.AuthUser(username, password);
        }

        public Task<SpartanUserList> AuthUserSpartane(string username, string password)
        {
            return sessionService.AuthUserSpartane(username, HelperEncrypt.EncryptPassword(password));
        }

        public Task<List<UserModel>> GetClients()
        {
            return sessionService.GetClients();
        }

        public Task<SpartaneFileModel> GetFile(long? id)
        {
            return sessionService.GetFile(id);
        }

        public Task<UserModel> GetUser(string username)
        {
            return sessionService.GetUser(username);
        }
    }
}
