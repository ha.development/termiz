﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Refit;
using TermizApp.Helpers;
using TermizApp.Models.Catalogs;
using TermizApp.Settings;

namespace TermizApp.Services.Catalogs
{
    public class TiempoRealService : ITiempoRealService
    {
        private readonly ITiempoRealService tiempoRealService;

        public TiempoRealService()
        {
            tiempoRealService = RestService.For<ITiempoRealService>(new HttpClient(new HttpLoggingHandler(TokenManager.GetToken)) { BaseAddress = new Uri(AppConfiguration.Values.BaseUrl) }, new RefitSettings
            {
                JsonSerializerSettings = new JsonSerializerSettings
                {
                    ContractResolver = new CustomResolver()
                }
            });
        }

        public Task<List<Vertica_ConsumoDemandaHoras>> GetConsumoTiempoReal(int idEquipo, int idVariable, int idRegion, int idSitio, string claveTag, long IdCliente, string filtroPeriodo)
        {
            return tiempoRealService.GetConsumoTiempoReal(idEquipo, idVariable, idRegion, idSitio, claveTag, IdCliente, filtroPeriodo);
        }

        public Task<List<Vertica_ConsumoDemandaHoras>> GetConsumoTiempoRealDia(int idEquipo, int idRegion, int idSitio, string claveTag, long IdCliente)
        {
            return tiempoRealService.GetConsumoTiempoRealDia(idEquipo, idRegion, idSitio, claveTag, IdCliente);
        }
        
        public Task<Vertica_ConsumoDemandaMovil> GetConsumoTiempoRealMovil(int idEquipo, int idRegion, int idSitio, string claveTag, long IdCliente)
        {
            return tiempoRealService.GetConsumoTiempoRealMovil(idEquipo, idRegion, idSitio, claveTag, IdCliente);
        }        

        public Task<List<Vertica_Odometros>> GetOdometros(int idEquipo, string idTags)
        {
            return tiempoRealService.GetOdometros(idEquipo, idTags);
        }


        public Task<List<Vertica_Tablas>> GetTablas(int idSitio, int idEquipo, bool isAdmin = false)
        {
            return tiempoRealService.GetTablas(idSitio, idEquipo, isAdmin);
        }

        public Task<List<Vertica_Variables>> GetVariablesTiempoRealMovil()
        {
            return tiempoRealService.GetVariablesTiempoRealMovil();
        }

        public Task<Vertica_ConsumoDemandaTiempoReal> GetConsumoTiempoRealPorPeriodo(int idEquipo, int idVariable, int idRegion, int idSitio, string claveTag, long IdCliente, string filtroPeriodo)
        {
            return tiempoRealService.GetConsumoTiempoRealPorPeriodo(idEquipo, idVariable, idRegion, idSitio, claveTag, IdCliente, filtroPeriodo);
        }

        public Task<int> GetTiempoActualizacionOdometros()
        {
            return tiempoRealService.GetTiempoActualizacionOdometros();
        }


    }
}
