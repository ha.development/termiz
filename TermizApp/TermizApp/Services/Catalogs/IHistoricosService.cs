﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Refit;
using TermizApp.Models.Catalogs;

namespace TermizApp.Services.Catalogs
{
    public interface IHistoricosService
    {

        [Get("/Vertica_Historicos/GetHistoricos")]
        [Headers("Authorization: Bearer")]
        Task<List<Vertica_Historicos>> GetHistoricos(int idSitio, int idEquipo, string idsVariablesCad, string fechaInicio,string fechaFin);

        [Get("/Vertica_Historicos/GetHistoricosHora")]
        [Headers("Authorization: Bearer")]
        Task<List<Vertica_Historicos>> GetHistoricosHora(int idSitio, int idEquipo, string idsVariablesCad);

        [Get("/Vertica_Historicos/GetHistoricosDia")]
        [Headers("Authorization: Bearer")]
        Task<List<Vertica_Historicos>> GetHistoricosDia(int idSitio, int idEquipo, string idsVariablesCad);

        [Get("/Vertica_Historicos/GetHistoricosMes")]
        [Headers("Authorization: Bearer")]
        Task<List<Vertica_Historicos>> GetHistoricosMes(int idSitio, int idEquipo, string idsVariablesCad);

        [Get("/Vertica_Historicos/GetHistoricosAnio")]
        [Headers("Authorization: Bearer")]
        Task<List<Vertica_Historicos>> GetHistoricosAnio(int idSitio, int idEquipo, string idsVariablesCad);

    }
}