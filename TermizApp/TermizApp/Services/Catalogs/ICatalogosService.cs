﻿using System.Threading.Tasks;
using Refit;
using TermizApp.Models.Catalogs;

namespace TermizApp.Services.Catalogs
{
    public interface ICatalogosService
    {


        [Get("/Vertica_Catalogos/GetRegiones")]
        [Headers("Authorization: Bearer")]
        Task<System.Collections.Generic.List<Vertica_Region>> GetRegiones(string username, long idCliente);


        [Get("/Vertica_Catalogos/GetSitios")]
        [Headers("Authorization: Bearer")]
        Task<System.Collections.Generic.List<Vertica_Sitio>> GetSitios(int idRegion, string username);


        [Get("/Vertica_Catalogos/GetEquipos")]
        [Headers("Authorization: Bearer")]
        Task<System.Collections.Generic.List<Vertica_Equipos>> GetEquipos(int idSitio, int idTablero);


        [Get("/Vertica_Catalogos/GetVariables")]
        [Headers("Authorization: Bearer")]
        Task<System.Collections.Generic.List<Vertica_Tags>> GetVariables(int idEquipo);



    }
}
