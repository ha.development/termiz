﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Refit;
using TermizApp.Helpers;
using TermizApp.Models.Catalogs;
using TermizApp.Settings;

namespace TermizApp.Services.Catalogs
{
    public class HistoricosService : IHistoricosService
    {
        private readonly IHistoricosService historicosService;

        public HistoricosService()
        {
            historicosService = RestService.For<IHistoricosService>(new HttpClient(new HttpLoggingHandler(TokenManager.GetToken)) { BaseAddress = new Uri(AppConfiguration.Values.BaseUrl) }, new RefitSettings
            {
                JsonSerializerSettings = new JsonSerializerSettings
                {
                    ContractResolver = new CustomResolver()
                }
            });
        }       

        public Task<List<Vertica_Historicos>> GetHistoricos(int idSitio, int idEquipo, string idsVariablesCad, string fechaInicio, string fechaFin)
        {
            return historicosService.GetHistoricos( idSitio,  idEquipo,  idsVariablesCad,  fechaInicio,  fechaFin);
        }

        public Task<List<Vertica_Historicos>> GetHistoricosHora(int idSitio, int idEquipo, string idsVariablesCad)
        {
            return historicosService.GetHistoricosHora(idSitio, idEquipo, idsVariablesCad);
        }

        public Task<List<Vertica_Historicos>> GetHistoricosDia(int idSitio, int idEquipo, string idsVariablesCad)
        {
            return historicosService.GetHistoricosDia(idSitio, idEquipo, idsVariablesCad);
        }

        public Task<List<Vertica_Historicos>> GetHistoricosMes(int idSitio, int idEquipo, string idsVariablesCad)
        {
            return historicosService.GetHistoricosMes(idSitio, idEquipo, idsVariablesCad);
        }

        public Task<List<Vertica_Historicos>> GetHistoricosAnio(int idSitio, int idEquipo, string idsVariablesCad)
        {
            return historicosService.GetHistoricosAnio(idSitio, idEquipo, idsVariablesCad);
        }
    }
}
