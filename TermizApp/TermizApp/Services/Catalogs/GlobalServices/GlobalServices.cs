﻿using Newtonsoft.Json;
using Refit;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using TermizApp.Models.Catalogs;
using TermizApp.Settings;

namespace TermizApp.Services.Catalogs.GlobalServices
{
    public class GlobalServices : IGlobalServices
    {

        private readonly IGlobalServices services;

        public GlobalServices()
        {
            services = RestService.For<IGlobalServices>(new HttpClient(new HttpLoggingHandler(TokenManager.GetToken)) { BaseAddress = new Uri(AppConfiguration.Values.BaseUrl) }, new RefitSettings
            {
                JsonSerializerSettings = new JsonSerializerSettings
                {
                    ContractResolver = new CustomResolver()
                }
            });
        }


        public Task<List<EquipmentModel>> GetEquipment(long idSite)
        {
            return services.GetEquipment(idSite);
        }

        public Task<List<RegionModel>> GetRegions(string username)
        {
            return services.GetRegions(username);
        }

        public Task<List<SiteModel>> GetSites(long idRegion, string username)
        {
            return services.GetSites(idRegion, username);
        }

        public Task<List<EquipmentStatesModel>> GetEquipmentStates(long idCliente)
        {
            return services.GetEquipmentStates(idCliente);
        }
    }
}
