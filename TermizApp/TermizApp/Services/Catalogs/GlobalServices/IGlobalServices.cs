﻿using Refit;
using System.Collections.Generic;
using System.Threading.Tasks;
using TermizApp.Models.Catalogs;

namespace TermizApp.Services.Catalogs.GlobalServices
{
    public interface IGlobalServices
    {
        [Get("/Vertica_Catalogos/GetRegiones")]
        [Headers("Authorization: Bearer")]
        Task<List<RegionModel>> GetRegions(string username);

        [Get("/Vertica_Catalogos/GetSitios")]
        [Headers("Authorization: Bearer")]
        Task<List<SiteModel>> GetSites(long idRegion, string username);

        [Get("/Vertica_Catalogos/GetEquipos")]
        [Headers("Authorization: Bearer")]
        Task<List<EquipmentModel>> GetEquipment(long idSitio);

        [Get("/Vertica_StartCenter/GetEstadoEquipos")]
        [Headers("Authorization: Bearer")]
        Task<List<EquipmentStatesModel>> GetEquipmentStates(long idCliente);
    }
}
