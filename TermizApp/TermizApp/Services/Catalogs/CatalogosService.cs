﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Refit;
using TermizApp.Helpers;
using TermizApp.Models.Catalogs;
using TermizApp.Settings;

namespace TermizApp.Services.Catalogs
{
    public class CatalogosService : ICatalogosService
    {
        private readonly ICatalogosService catalogosService;

        public CatalogosService()
        {
            catalogosService = RestService.For<ICatalogosService>(new HttpClient(new HttpLoggingHandler(TokenManager.GetToken)) { BaseAddress = new Uri(AppConfiguration.Values.BaseUrl) }, new RefitSettings
            {
                JsonSerializerSettings = new JsonSerializerSettings
                {
                    ContractResolver = new CustomResolver()
                }
            });
        }



        public Task<List<Vertica_Region>> GetRegiones(string username, long idCliente)
        {
            return catalogosService.GetRegiones(username, idCliente);
        }


        public Task<List<Vertica_Sitio>> GetSitios(int idRegion, string username)
        {
            return catalogosService.GetSitios(idRegion, username);
        }

        public Task<List<Vertica_Equipos>> GetEquipos(int idSitio, int idTablero)
        {
            return catalogosService.GetEquipos(idSitio, idTablero);
        }


        public Task<List<Vertica_Tags>> GetVariables(int idEquipo)
        {
            return catalogosService.GetVariables(idEquipo);
        }

    }
}
