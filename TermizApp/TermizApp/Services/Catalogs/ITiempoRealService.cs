﻿using System.Threading.Tasks;
using Refit;
using TermizApp.Models.Catalogs;

namespace TermizApp.Services.Catalogs
{
    public interface ITiempoRealService
    {

        [Get("/Vertica_TiempoReal/GetConsumoTiempoReal")]
        [Headers("Authorization: Bearer")]
        Task<System.Collections.Generic.List<Vertica_ConsumoDemandaHoras>> GetConsumoTiempoReal(int idEquipo, int idVariable, int idRegion, int idSitio, string claveTag, long IdCliente, string filtroPeriodo);


        [Get("/Vertica_TiempoReal/GetConsumoTiempoRealDia")]
        [Headers("Authorization: Bearer")]
        Task<System.Collections.Generic.List<Vertica_ConsumoDemandaHoras>> GetConsumoTiempoRealDia(int idEquipo, int idRegion, int idSitio, string claveTag, long IdCliente);

        
        [Get("/Vertica_TiempoReal/GetConsumoTiempoRealMovil")]
        [Headers("Authorization: Bearer")]
        Task<Vertica_ConsumoDemandaMovil> GetConsumoTiempoRealMovil(int idEquipo, int idRegion, int idSitio, string claveTag, long IdCliente);       
        

        [Get("/Vertica_TiempoReal/GetOdometros")]
        [Headers("Authorization: Bearer")]
        Task<System.Collections.Generic.List<Vertica_Odometros>> GetOdometros(int idEquipo, string idTags);

        [Get("/Vertica_TiempoReal/GetTablas")]
        [Headers("Authorization: Bearer")]
        Task<System.Collections.Generic.List<Vertica_Tablas>> GetTablas(int idSitio, int idEquipo, bool isAdmin = false);

        [Get("/Vertica_TiempoReal/GetVariablesTiempoRealMovil")]
        [Headers("Authorization: Bearer")]
        Task<System.Collections.Generic.List<Vertica_Variables>> GetVariablesTiempoRealMovil();

        [Get("/Vertica_TiempoReal/GetConsumoTiempoRealPorPeriodo")]
        [Headers("Authorization: Bearer")]
        Task<Vertica_ConsumoDemandaTiempoReal> GetConsumoTiempoRealPorPeriodo(int idEquipo, int idVariable, int idRegion, int idSitio, string claveTag, long IdCliente, string filtroPeriodo);

        [Get("/Vertica_TiempoReal/GetTiempoActualizacionOdometros")]
        [Headers("Authorization: Bearer")]
        Task<int> GetTiempoActualizacionOdometros();
    }
}