﻿using TermizApp.Controls.Bases;
using Xamarin.Forms.Xaml;

namespace TermizApp.Views.StartCenter
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class StartCenterPage : ContentPageBase
	{
		public StartCenterPage()
		{
            InitializeComponent();
		}

        protected override void OnAppearing()
        {
            base.OnAppearing();
            map.PinClicked += (sender, e) =>
            {
                e.Pin.Label = null;
                e.Pin.Address = null;
                map.SelectedPin = null;
            };
        }

    }
}