using TermizApp.Controls.Bases;
using Xamarin.Forms;

namespace TermizApp.Views.Home
{
    public partial class TiempoReal : ContentPageBase
    {
        double x;
        int posicion = 0;


        public TiempoReal()
        {
            InitializeComponent();

            Pagina1.IsVisible = true;
            Pagina2.IsVisible = false;
            Pagina3.IsVisible = false;
        }


        void OnPanUpdated(object sender, PanUpdatedEventArgs e)
        {
            switch (e.StatusType)
            {
                case GestureStatus.Started:
                    x = e.TotalX;
                    break;

                case GestureStatus.Running:
                    x = e.TotalX;
                    break;

                case GestureStatus.Completed:
                    x = e.TotalX - x;

                    if (x > 100)
                        Avanza();
                    if (x < -100)
                        Regresa();
                    break;
            }
        }

        async void Regresa()
        {
            if (posicion <= 0)
                return;

 

            switch (posicion)
            {
                case 1:
                    ScrollTo(0);
                    break;
                case 2:
                    ScrollTo(1);
                    break;
                case 3:
                  ScrollTo(2);
                    break;
            }

            posicion--;
        }

        async void Avanza()
        {
            if (posicion >= 3)
                return;



            switch (posicion)
            {
                case 0:
                    ScrollTo(1);
                    break;
                case 1:
                    ScrollTo(2);
                    break;
                case 2:
                    ScrollTo(3);
                    break;
            }

            posicion++;
        }


        void ScrollTo(int d)
        {
            switch (d)                                                                         
            {
                case 0:
                    opcion1.Source = "ic_active_check.png";
                    opcion2.Source = "ic_notactive_check.png";
                    opcion3.Source = "ic_notactive_check.png";
                    Pagina1.IsVisible = true;
                    Pagina2.IsVisible = false;
                    Pagina3.IsVisible = false;
                    return;

                case 1:
                    opcion1.Source = "ic_notactive_check.png";
                    opcion2.Source = "ic_active_check.png";
                    opcion3.Source = "ic_notactive_check.png";
                    Pagina1.IsVisible = false;
                    Pagina2.IsVisible = true;
                    Pagina3.IsVisible = false;
                    return;


                case 2:
                    opcion1.Source = "ic_notactive_check.png";
                    opcion2.Source = "ic_notactive_check.png";
                    opcion3.Source = "ic_active_check.png";
                    Pagina1.IsVisible = false;
                    Pagina2.IsVisible = false;
                    Pagina3.IsVisible = true;
                    return;
            }
        }

        private void Opcion1_Clicked(object sender, System.EventArgs e)
        {
            ScrollTo(0);
        }

        private void Opcion2_Clicked(object sender, System.EventArgs e)
        {
            ScrollTo(1);
        }

        private void Opcion3_Clicked(object sender, System.EventArgs e)
        {
            ScrollTo(2);
        }
    }
}
