﻿using Xamarin.Forms;

namespace TermizApp.Views.Home
{
    public partial class MasterPage : MasterDetailPage
    {
        public MasterPage()
        {
            InitializeComponent();
            App.Master = this;
            NavigationPage.SetHasNavigationBar(this, false);
        }
    }
}
