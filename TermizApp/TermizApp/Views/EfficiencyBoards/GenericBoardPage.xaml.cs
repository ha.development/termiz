﻿using TermizApp.Controls.Bases;
using Xamarin.Forms.Xaml;

namespace TermizApp.Views.EfficiencyBoards
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class GenericBoardPage : ContentPageBase
	{
		public GenericBoardPage ()
		{
			InitializeComponent ();
		}
	}
}