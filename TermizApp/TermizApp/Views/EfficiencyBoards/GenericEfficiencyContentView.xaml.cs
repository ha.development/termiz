﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TermizApp.Views.EfficiencyBoards
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class GenericEfficiencyContentView : ContentView
	{
		public GenericEfficiencyContentView ()
		{
			InitializeComponent ();
		}
	}
}