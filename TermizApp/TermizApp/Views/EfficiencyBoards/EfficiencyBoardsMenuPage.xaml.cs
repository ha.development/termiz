﻿using TermizApp.Controls.Bases;
using Xamarin.Forms.Xaml;

namespace TermizApp.Views.EfficiencyBoards
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EfficiencyBoardsMenuPage : ContentPageBase
	{
		public EfficiencyBoardsMenuPage ()
		{
			InitializeComponent ();
		}
	}
}