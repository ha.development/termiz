﻿using Rg.Plugins.Popup.Pages;
using Xamarin.Forms.Xaml;

namespace TermizApp.Views.Popups
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DetailEquimentPopup : PopupPage
    {
		public DetailEquimentPopup ()
		{
            InitializeComponent();
		}
	}
}