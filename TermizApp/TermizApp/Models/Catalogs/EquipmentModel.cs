﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace TermizApp.Models.Catalogs
{
    public class EquipmentListModel
    {
        public List<EquipmentModel> Hardware { get; set; }
    }
    public class EquipmentModel
    {
        [JsonProperty("Id")]
        public long Id { get; set; }

        [JsonProperty("ClaveEquipo")]
        public string ClaveEquipment { get; set; }

        [JsonProperty("IdArquitectura")]
        public long IdArchitecture { get; set; }

        [JsonProperty("IdSitio")]
        public long IdSite { get; set; }

        [JsonProperty("IdSubsistema")]
        public long IdSubSystem { get; set; }

        [JsonProperty("IdTipoEquipo")]
        public long IdTypeEquipment { get; set; }

        [JsonProperty("Nombre")]
        public string Name { get; set; }

        [JsonProperty("ObjetoDeServicio")]
        public string ObjetOfService { get; set; }

        [JsonProperty("FechaFabricacion")]
        public DateTimeOffset DateMake { get; set; }

        [JsonProperty("Marca")]
        public string Brand { get; set; }

        [JsonProperty("Modelo")]
        public string Model { get; set; }

        [JsonProperty("NumeroSerie")]
        public string NumberSeries { get; set; }

        [JsonProperty("Capacidad")]
        public long Capacity { get; set; }

        [JsonProperty("Activo")]
        public bool Active { get; set; }
    }
}
