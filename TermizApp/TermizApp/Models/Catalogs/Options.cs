﻿using System;
namespace TermizApp.Models.Catalogs
{
    public class Options
    {
        public int Id { get; set; }
        public string Clave { get; set; }
        public string Nombre { get; set; }
        public bool Selected { get; set; }
        public string Imagen { get; set; }
    }
}
