﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace TermizApp.Models.Catalogs
{
    public class SiteListModel
    {
        public List<SiteModel> Site { get; set; }
    }
    public class SiteModel
    {
        [JsonProperty("Id")]
        public long Id { get; set; }

        [JsonProperty("IdContacto")]
        public long IdContact { get; set; }

        [JsonProperty("Nombre")]
        public string Name { get; set; }

        [JsonProperty("IdSubclasificacionSitio")]
        public long IdSubClasificationSite { get; set; }

        [JsonProperty("IdPoblacion")]
        public long IdPopulation { get; set; }

        [JsonProperty("Domicilio")]
        public string Address { get; set; }

        [JsonProperty("CodigoPostal")]
        public long CP { get; set; }

        [JsonProperty("Latitud")]
        public double Lat { get; set; }

        [JsonProperty("Longitud")]
        public double Long { get; set; }

        [JsonProperty("IdRegion")]
        public long IdRegion { get; set; }
    }
}
