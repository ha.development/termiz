﻿using Newtonsoft.Json;
using System;
namespace TermizApp.Models.Catalogs
{
    public class Vertica_Odometros
    {
        [JsonProperty("IdTag")]
        public long IdTag { get; set; }

        [JsonProperty("IdEquipo")]
        public long IdEquipo { get; set; }

        [JsonProperty("Tag")]
        public string Tag { get; set; }

        [JsonProperty("Equipo")]
        public string Equipo { get; set; }

        [JsonProperty("TagValue")]
        public float TagValue { get; set; }

        [JsonProperty("PorcetajeDesviacion")]
        public float PorcetajeDesviacion { get; set; }

        [JsonProperty("LimiteMax")]
        public long LimiteMax { get; set; }

        [JsonProperty("LimiteMin")]
        public long LimiteMin { get; set; }

        [JsonProperty("ValorObjetivo")]
        public long ValorObjetivo { get; set; }

        [JsonProperty("MaxValue")]
        public long MaxValue { get; set; }

        [JsonProperty("MinValue")]
        public long MinValue { get; set; }

        [JsonProperty("UnidadMedida")]
        public string UnidadMedida { get; set; }

        [JsonProperty("Color")]
        public string Color { get; set; }

        [JsonProperty("ColorRangoMenorValorObjetivo")]
        public string ColorRangoMenorValorObjetivo { get; set; }

        [JsonProperty("ColorRangoMayorValorObjetivo")]
        public string ColorRangoMayorValorObjetivo { get; set; }

        [JsonProperty("Ajuste")]
        public long Ajuste { get; set; }

        [JsonProperty("Categoria")]
        public string Categoria { get; set; }

        [JsonProperty("IdCategoria")]
        public long IdCategoria { get; set; }

        [JsonProperty("TagTimestamp")]
        public DateTimeOffset TagTimestamp { get; set; }
    }
}
