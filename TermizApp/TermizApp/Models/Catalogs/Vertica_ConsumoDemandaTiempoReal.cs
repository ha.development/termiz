﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TermizApp.Models.Catalogs
{
    public class Vertica_ConsumoDemandaTiempoReal
    {
        public Vertica_ConsumoDemandaHoras DatosTag { get; set; }
        public float[] Actual { get; set; }
        public float[] Anterior { get; set; }
    }

    
}
