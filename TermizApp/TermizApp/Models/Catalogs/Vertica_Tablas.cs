﻿using System;
namespace TermizApp.Models.Catalogs
{
    public class Vertica_Tablas
    {
        public int IdTag { get; set; }
        public string Tag { get; set; }
        public int IdSitio { get; set; }
        public string Sitio { get; set; }
        public int IdEquipo { get; set; }
        public string Equipo { get; set; }
        public int IdComponente { get; set; }
        public string Componente { get; set; }
        public int IdTaxonomia { get; set; }
        public string Taxonomia { get; set; }
        public float TagValue { get; set; }
        public float PorcetajeDesviacion { get; set; }
        public float LimiteMax { get; set; }
        public float LimiteMin { get; set; }
        public float ValorObjetivo { get; set; }
        public float MaxValue { get; set; }
        public float MinValue { get; set; }
        public string UnidadMedida { get; set; }
        public string Color { get; set; }
        public int Ajuste { get; set; }
        public string Clave { get; set; }
        public string Variable { get; set; }
    }
}
