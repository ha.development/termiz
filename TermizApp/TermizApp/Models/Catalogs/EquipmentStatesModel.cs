﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;

namespace TermizApp.Models.Catalogs
{
    public class EquipmentStatesModel
    {
        [JsonProperty("Nombre")]
        public string Nombre { get; set; }

        [JsonProperty("Latitud")]
        public double Latitud { get; set; }

        [JsonProperty("Longitud")]
        public double Longitud { get; set; }

        [JsonProperty("Eventos")]
        public List<EventEquipment> Eventos { get; set; }

        [JsonIgnore]
        public Position Position { get; set; }

        [JsonIgnore]
        public BitmapDescriptor Icon { get; set; }

        [JsonProperty("Color")]
        public long Color { get; set; }

        [JsonProperty("IdEstado")]
        public long IdState { get; set; }
    }

    public class EventEquipment
    {
        [JsonProperty("Nombre")]
        public string Nombre { get; set; }

        [JsonProperty("Frecuencia")]
        public string Frecuencia { get; set; }

        [JsonProperty("Nivel")]
        public long? Nivel { get; set; }

        [JsonProperty("FechaInicio")]
        public DateTime? FechaInicio { get; set; }

        [JsonProperty("FechaFin")]
        public DateTime? FechaFin { get; set; }

        [JsonProperty("Fecha")]
        public DateTime? Fecha { get; set; }

        [JsonIgnore]
        public string DescripcionFecha { get; set; }

        [JsonIgnore]
        public Color ColorStatus { get; set; }

        [JsonProperty("ColorSeveridad")]
        public string ColorSeveridad { get; set; }

        [JsonIgnore]
        public string Image { get; set; }
    }
}
