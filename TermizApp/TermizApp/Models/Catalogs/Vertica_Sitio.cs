﻿using System;
namespace TermizApp.Models.Catalogs
{
    public class Vertica_Sitio
    {

            public int Id { get; set; }
            public int IdContacto { get; set; }
            public string Nombre { get; set; }
            public int IdSubclasificacionSitio { get; set; }
            public int IdPoblacion { get; set; }
            public string Domicilio { get; set; }
            public int CodigoPostal { get; set; }
            public decimal Latitud { get; set; }
            public decimal Longitud { get; set; }
            public int IdRegion { get; set; }
    
    }
}
