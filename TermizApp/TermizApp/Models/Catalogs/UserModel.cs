﻿using Newtonsoft.Json;

namespace TermizApp.Models.Catalogs
{
    public class UserModel
    {
        [JsonProperty("Id")]
        public long Id { get; set; }

        [JsonProperty("Nombre")]
        public string Nombre { get; set; }

        [JsonProperty("IdCliente")]
        public long? IdCliente { get; set; }

        [JsonProperty("Email")]
        public string Email { get; set; }

        [JsonProperty("IdRol")]
        public long? IdRol { get; set; }

        [JsonProperty("Language")]
        public string Language { get; set; }

        [JsonProperty("IconoLogo")]
        public long? IconoLogo { get; set; }
    }
}
