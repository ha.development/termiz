﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace TermizApp.Models.Catalogs
{
    public class RegionListModel
    {
        public List<RegionModel> Regions { get; set; }
    }
    public class RegionModel
    {
        [JsonProperty("Id")]
        public long Id { get; set; }

        [JsonProperty("Nombre")]
        public string Name { get; set; }

        [JsonProperty("IdCliente")]
        public long IdClient { get; set; }

        [JsonProperty("Latitud")]
        public long Lat { get; set; }

        [JsonProperty("Longitud")]
        public long Long{ get; set; }
    }
}
