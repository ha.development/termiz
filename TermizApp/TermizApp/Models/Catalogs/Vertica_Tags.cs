﻿using System;
namespace TermizApp.Models.Catalogs
{
    public class Vertica_Tags
    {

        public int IdTag { get; set; }
        public string Tag { get; set; }
        public int IdEquipo { get; set; }
        public string Equipo { get; set; }
        public int IdArquitectura { get; set; }
        public string Arquitectura { get; set; }
        public int IdComponente { get; set; }
        public string Componente { get; set; }
        public int IdTaxonomia { get; set; }
        public string Taxonomia { get; set; }
        public string TipoTag { get; set; }
        public float ValorObjetivo { get; set; }
        public bool FlagEvento { get; set; }
        public bool FlagTiempoReal { get; set; }
        public bool FlagHistorico { get; set; }
        public float ValorAnalogico { get; set; }
        public float ValorEscalado { get; set; }
        public float MaxValue { get; set; }
        public float MinValue { get; set; }
        public int IdUnidad { get; set; }
        public string Unidad { get; set; }
        public int IdSistemaMedicion { get; set; }
        public string SistemaMedicion { get; set; }
        public bool Selected { get; set; }

    }
}
