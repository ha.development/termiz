﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using TermizApp.Abstractions;

namespace TermizApp.Models.Catalogs
{

    public partial class ConsumoDemandatList 
    {
        [JsonProperty("Vertica_ConsumoDemandaHoras")]
        public List<ProgressReportModel> Vertica_ConsumoDemandaHoras { get; set; }
    }

    public partial class ConsumoDemandatModel 
    {
        [JsonProperty("IdTag")]
        public int IdTag { get; set; }

        [JsonProperty("IdEquipo")]
        public int IdEquipo { get; set; }

        [JsonProperty("Tag")]
        public string Tag { get; set; }

        [JsonProperty("TimeStamp")]
        public DateTime TimeStamp { get; set; }

        [JsonProperty("TagValue")]
        public float TagValue { get; set; }

        [JsonProperty("NoHora")]
        public int NoHora { get; set; }




    }
}
