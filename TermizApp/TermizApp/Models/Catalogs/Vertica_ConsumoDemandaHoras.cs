﻿using System;
namespace TermizApp.Models.Catalogs
{
    public class Vertica_ConsumoDemandaHoras
    {
        public int? IdTag { get; set; }
        public int IdEquipo { get; set; }
        public string Tag { get; set; }
        public DateTime TimeStamp { get; set; }
        public float TagValue { get; set; }
        public int? NoHora { get; set; }
        public int? NoDia { get; set; }
        public int? NoMes { get; set; }
        public int? NoAnio { get; set; }
        public string ClaveTag { get; set; }
        public string UnidadIngenieria { get; set; }
    }
}
