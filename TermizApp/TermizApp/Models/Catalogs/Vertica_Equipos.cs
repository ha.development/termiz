﻿using System;
namespace TermizApp.Models.Catalogs
{
    public class Vertica_Equipos
    {
        public int Id { get; set; }
        public string ClaveEquipo { get; set; }
        public int IdArquitectura { get; set; }
        public int IdSitio { get; set; }
        public int IdSubsistema { get; set; }
        public int IdTipoEquipo { get; set; }
        public string Nombre { get; set; }
        public string ObjetoDeServicio { get; set; }
        public DateTime FechaFabricacion { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public string NumeroSerie { get; set; }
        public float Capacidad { get; set; }
        public bool Activo { get; set; }

    }
}
