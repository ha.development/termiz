﻿using System;
namespace TermizApp.Models.Catalogs
{
    public class Vertica_Region
    {
            public int Id { get; set; }
            public string Nombre { get; set; }
            public int IdCliente { get; set; }
            public decimal Latitud { get; set; }
            public decimal Longitud { get; set; }
    }
}
