﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TermizApp.Models.Catalogs
{
    public class Vertica_ConsumoDemandaMovil
    {
        public List<Vertica_ConsumoDemandaHoras> Ayer { get; set; }
        public List<Vertica_ConsumoDemandaHoras> Hoy { get; set; }
    }
}
