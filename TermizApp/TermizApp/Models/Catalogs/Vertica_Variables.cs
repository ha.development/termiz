﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TermizApp.Models.Catalogs
{
    public class Vertica_Variables
    {
        public int IdTag { get; set; }
        public string Clave { get; set; }        
        public int? IdSitio { get; set; }
        public int? IdEquipo { get; set; }
        public string NombreEquipo { get; set; }
        public string Variable { get; set; }       
        public string Componente { get; set; }
        public string UnidadMedicion { get; set; }
        public string Valor { get; set; }
        public string Color { get; set; }
        public int? Id { get; set; }       
    }
}
