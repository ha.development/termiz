﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using TermizApp.Abstractions;

namespace TermizApp.Models.Catalogs
{
    public partial class ProgressReportList 
    {
        [JsonProperty("Progress_Reports")]
        public List<ProgressReportModel> ProgressReports { get; set; }

        [JsonProperty("RowCount")]
        public int RowCount { get; set; }
    }

    public partial class ProgressReportModel 
    {
        [JsonProperty("ReportId")]
        public int ReportId { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }
    }
}
