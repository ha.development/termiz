﻿using System;
namespace TermizApp.Models.Catalogs
{
    public class Vertica_ConsumoDemandaHistorico
    {
        public int IdTag { get; set; }
        public int IdEquipo { get; set; }
        public string Tag { get; set; }
        public DateTime TimeStamp { get; set; }
        public float TagValue { get; set; }
        public string NoHora { get; set; }

    }
}
