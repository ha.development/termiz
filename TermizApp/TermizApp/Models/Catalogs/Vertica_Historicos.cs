﻿using Newtonsoft.Json;
using System;
namespace TermizApp.Models.Catalogs
{
    public partial class Vertica_Historicos
    {
        [JsonProperty("NombreVariable")]
        public string NombreVariable { get; set; }

        [JsonProperty("Unidad")]
        public string Unidad { get; set; }

        [JsonProperty("PuntoMinimoDeDatos")]
        public double PuntoMinimoDeDatos { get; set; }

        [JsonProperty("PuntoMinimoAceptacion")]
        public long PuntoMinimoAceptacion { get; set; }

        [JsonProperty("PuntoMaximoAceptacion")]
        public long PuntoMaximoAceptacion { get; set; }

        [JsonProperty("PuntoMaximoDeDatos")]
        public double PuntoMaximoDeDatos { get; set; }

        [JsonProperty("ColorDeBajos")]
        public string ColorDeBajos { get; set; }

        [JsonProperty("ColorDeAltos")]
        public string ColorDeAltos { get; set; }

        [JsonProperty("Puntos")]
        public Punto[] Puntos { get; set; }
    }

    public partial class Punto
    {
        [JsonProperty("Fecha")]
        public DateTimeOffset Fecha { get; set; }

        [JsonProperty("Valor")]
        public float Valor { get; set; }
    }
}
