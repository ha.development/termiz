﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TermizApp.Models.Temp
{
    public class HomeClassTemp
    {
        public string Title { get; set; }
        public string TitleCount { get; set; }
        public double Progress { get; set; }
    }
}
