﻿namespace TermizApp.Models.Bases
{

    public enum Fonts
    {
        Ubuntu,
        QuicksandLight,
        None
    }

    public enum Icons
    {
        Regular,
        Solid,
        BrandsRegular,
        Light
    }

}
