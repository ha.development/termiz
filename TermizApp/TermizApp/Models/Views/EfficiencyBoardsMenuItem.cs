﻿namespace TermizApp.Models.Views
{
    public class EfficiencyBoardsMenuItem
    {
        public string Title { get; set; }
        public TypeBoard Board { get; set; }
        public string Image { get; set; }
    }
    public enum TypeBoard
    {
        ExecutiveBoard,
        OperativeBoard,
        TechnicalBoard
    }
}
